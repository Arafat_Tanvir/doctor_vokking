<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[
	'uses'=>'HomeController@index',
	'as'=>'public'
]);

Route::get('/news',[
	'uses'=>'HomeController@news',
	'as'=>'news'
]);

Route::get('/services',[
	'uses'=>'HomeController@services',
	'as'=>'services'
]);

Route::get('/contact',[
	'uses'=>'HomeController@contact',
	'as'=>'contact'
]);

Route::get('/about_us',[
	'uses'=>'HomeController@about_us',
	'as'=>'about_us'
]);

Route::get('/doctor_veiw',[
	'uses'=>'HomeController@doctor_veiw',
	'as'=>'doctor_veiw'
]);

Route::post('/search_schedule',[
	'uses'=>'HomeController@search_schedule',
	'as'=>'search_schedule'
]);

Auth::routes();

Route::get('/request_to_booking/{id}',[
	'uses'=>'OrderController@request_to_booking',
	'as'=>'request_to_booking'
]);
Route::post('/order',[
	'uses'=>'OrderController@order',
	'as'=>'order'
]);

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin'],function(){
        Route::get('/', [
			'uses' => 'Admin\AdminController@dashboard',
			'as' => 'admin.dashboard'
		]);
		Route::get('/login',[
			'uses'=>'Auth\Admin\LoginController@showLoginForm',
			'as'=>'admin.login'
		]);
		Route::post('/login/submit',[
			'uses'=>'Auth\Admin\LoginController@login',
			'as'=>'admin.login.submit'
		]);
		Route::post('/logout',[
			'uses'=>'Auth\Admin\LoginController@logout',
			'as'=>'admin.logout'
	    ]);
	    //admin Forgotpassword and reset password
		Route::post('/password/email',[
			'uses'=>'Auth\Admin\ForgotPasswordController@sendResetLinkEmail',
			'as'=>'admin.password.email'
		]);
		Route::get('/password/reset',[
			'uses'=>'Auth\Admin\ForgotPasswordController@showLinkRequestForm',
			'as'=>'admin.password.request'
		]);
		Route::get('/password/reset/{token}',[
			'uses'=>'Auth\Admin\ResetPasswordController@showResetForm',
			'as'=>'admin.password.reset'
		]);
		Route::post('/password/reset',[
			'uses'=>'Auth\Admin\ResetPasswordController@reset',
			'as'=>'admin.password.reset.post'
		]);
		//For category Routes
		Route::group(['prefix'=>'categories'],function(){
		    Route::get('/',"Admin\CategoryController@index")->name('categories_admin_index');
		    Route::get('/create',"Admin\CategoryController@create")->name('categories_admin_create');
		    Route::post('/store',"Admin\CategoryController@store")->name('categories_admin_store');
		    Route::get('/show/{id}',"Admin\CategoryController@show")->name('categories_admin_show');
		    Route::get('/edit/{id}',"Admin\CategoryController@edit")->name('categories_admin_edit');
		    Route::post('/update/{id}',"Admin\CategoryController@update")->name('categories_admin_update');
		    Route::post('/delete/{id}',"Admin\CategoryController@delete")->name('categories_admin_delete');
		});

		//For category Routes
		Route::group(['prefix'=>'medicals'],function(){
		    Route::get('/',"Admin\MedicalController@index")->name('medicals_admin_index');
		    Route::get('/create',"Admin\MedicalController@create")->name('medicals_admin_create');
		    Route::post('/store',"Admin\MedicalController@store")->name('medicals_admin_store');
		    Route::get('/show/{id}',"Admin\MedicalController@show")->name('medicals_admin_show');
		    Route::get('/edit/{id}',"Admin\MedicalController@edit")->name('medicals_admin_edit');
		    Route::post('/update/{id}',"Admin\MedicalController@update")->name('medicals_admin_update');
		    Route::post('/delete/{id}',"Admin\MedicalController@delete")->name('medicals_admin_delete');
		});

		//For category Routes
		Route::group(['prefix'=>'floors'],function(){
		    Route::get('/',"Admin\FloorController@index")->name('floors_admin_index');
		    Route::get('/create',"Admin\FloorController@create")->name('floors_admin_create');
		    Route::post('/store',"Admin\FloorController@store")->name('floors_admin_store');
		    Route::get('/show/{id}',"Admin\FloorController@show")->name('floors_admin_show');
		    Route::get('/edit/{id}',"Admin\FloorController@edit")->name('floors_admin_edit');
		    Route::post('/update/{id}',"Admin\FloorController@update")->name('floors_admin_update');
		    Route::post('/delete/{id}',"Admin\FloorController@delete")->name('floors_admin_delete');
		});

		//For category Routes
		Route::group(['prefix'=>'rooms'],function(){
		    Route::get('/',"Admin\RoomController@index")->name('rooms_admin_index');
		    Route::get('/create',"Admin\RoomController@create")->name('rooms_admin_create');
		    Route::post('/store',"Admin\RoomController@store")->name('rooms_admin_store');
		    Route::get('/show/{id}',"Admin\RoomController@show")->name('rooms_admin_show');
		    Route::get('/edit/{id}',"Admin\RoomController@edit")->name('rooms_admin_edit');
		    Route::post('/update/{id}',"Admin\RoomController@update")->name('rooms_admin_update');
		    Route::post('/delete/{id}',"Admin\RoomController@delete")->name('rooms_admin_delete');
		});

		//for all categories route
		Route::group(['prefix'=>'doctors'],function(){
				Route::get('/', [
					'uses' => 'Admin\DoctorController@index',
					'as' => 'doctor-admin-index'
				]);
				Route::get('/create', [
					'uses' => 'Admin\DoctorController@create',
					'as' => 'doctor-admin-create'
				]);
				Route::post('/store', [
					'uses' => 'Admin\DoctorController@store',
					'as' => 'doctor-admin-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Admin\DoctorController@edit',
					'as'=>'doctor-admin-edit'
				]);
				Route::post('/update/{id}',[
					'uses'=>'Admin\DoctorController@update',
					'as'=>'doctor-admin-update'
				]);
				Route::get('/show/{id}',[
					'uses'=>'Admin\DoctorController@show',
					'as'=>'doctor-admin-show'
				]);
				Route::get('/delete/{id}',[
					'uses'=>'Admin\DoctorController@delete',
					'as'=>'doctor-admin-delete'
				]);
		    });

		Route::group(['prefix'=>'schedules'],function(){
				Route::get('/', [
					'uses' => 'Admin\ScheduleController@index',
					'as' => 'schedule-admin-index'
				]);
				Route::get('/create', [
					'uses' => 'Admin\ScheduleController@create',
					'as' => 'schedule-admin-create'
				]);
				Route::post('/store', [
					'uses' => 'Admin\ScheduleController@store',
					'as' => 'schedule-admin-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Admin\ScheduleController@edit',
					'as'=>'schedule-admin-edit'
				]);
				Route::post('/update/{id}',[
					'uses'=>'Admin\ScheduleController@update',
					'as'=>'schedule-admin-update'
				]);
				Route::get('/show/{id}',[
					'uses'=>'Admin\ScheduleController@show',
					'as'=>'schedule-admin-show'
				]);
				Route::get('/delete/{id}',[
					'uses'=>'Admin\ScheduleController@delete',
					'as'=>'schedule-admin-delete'
				]);
		    });

		Route::group(['prefix'=>'orders'],function(){
				Route::get('/', [
					'uses' => 'Admin\OrderController@index',
					'as' => 'order-admin-index'
				]);
				Route::get('/create', [
					'uses' => 'Admin\OrderController@create',
					'as' => 'order-admin-create'
				]);
				Route::post('/store', [
					'uses' => 'Admin\OrderController@store',
					'as' => 'order-admin-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Admin\OrderController@edit',
					'as'=>'order-admin-edit'
				]);
				Route::post('/update/{id}',[
					'uses'=>'Admin\OrderController@update',
					'as'=>'order-admin-update'
				]);
				Route::get('/show/{id}',[
					'uses'=>'Admin\OrderController@show',
					'as'=>'order-admin-show'
				]);
				
				Route::get('/delete/{id}',[
					'uses'=>'Admin\OrderController@delete',
					'as'=>'order-admin-delete'
				]);
				Route::post('/order-complete/{id}',[
					'uses'=>'Admin\OrderController@order_complete',
					'as'=>'order_complete'
				]);
		    });
});


	//for load floors by ajax
	Route::get('/floors/ajax/{id}', [
		'uses' => 'Admin\FloorController@floors_ajax',
		'as' => 'floors'
	]);
	//for load rooms by ajax
	Route::get('/rooms/ajax/{id}', [
		'uses' => 'Admin\RoomController@rooms_ajax',
		'as' => 'rooms'
	]);

	Route::get('/rooms_search_ajax/ajax/{id}', [
		'uses' => 'Admin\RoomController@rooms_search_ajax',
		'as' => 'rooms_search_ajax'
	]);

	Route::get('/rooms_sub_ajax/ajax/{id}', [
		'uses' => 'Admin\RoomController@rooms_sub_ajax',
		'as' => 'rooms_sub_ajax'
	]);

