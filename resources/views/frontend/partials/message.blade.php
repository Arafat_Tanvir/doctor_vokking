
<div class="text-center" style="position: relative;padding-top: 30px;" >
	@if(Session::has('success'))
	<div class="alert alert-success Message" id="Message">
		<p>
			{{Session::get('success')}}
		</p>
	</div>
	@endif
</div>
<div class="text-center" style="position: relative;padding-top: 30px;">
	@if(Session::has('sticky_error'))
	<div class="alert alert-danger Message" id="Message">
		<p>
			{{Session::get('sticky_error')}}
		</p>
	</div>
	@endif
</div>
