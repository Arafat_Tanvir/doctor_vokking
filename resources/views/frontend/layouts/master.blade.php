<!DOCTYPE html>
<html lang="en">
<head>
<title>Doctor Management System</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('/')}}frontend/css/style.css">
</head>
<body style="background-image: url({{ route('public') }}/frontend/images/home_background_1.jpg);">

		    <div class="header-section" style="position: relative;height: 60px; padding-top: 10px; background-color: green">
								<h4 class="text-left" style="color: pink;font-style: italic;font-size: 30px">Synchronise It</h4>
			</div>
	        <div class="container">
	        	
				<div class="row">
					<div class="col-sm-12">

						@include('frontend.partials.message')
						@yield('content')
					</div>
			    </div>
			</div>
			

@include('frontend.partials.scripts')
@yield('scripts')
<script>
        $(function() {
            setTimeout(function() {
                $('#Message').delay(1000).fadeOut('slow');
            }, 1000);
        });
    </script>
</body>
</html>