@extends('frontend.layouts.master')
@section('title')
    Home
@endsection
@section('content')
			<div class="row justify-content-center" style="position: relative;padding-top: 20px;">

				<!-- About Content -->
				<div class="col-lg-7 card-header" style="position: relative;padding-bottom: 40px;">

					<form method="post" action="{{ route('order') }}">
						{{ csrf_field()}}
						<input name="schedule_id" class="d-none" value="{{$schedule->id}}" id="schedule_id">
						<input name="doctor_id" class="d-none" value="{{$schedule->doctor->id}}" id="doctor_id">
		                <div class="form-group row">
						    <label for="order_name" class="col-sm-4">Name</label>
						    <div class="col-sm-8">
						      <input type="text" name="order_name" class="form-control" id="order_name">
						    
						    @if ($errors->has('order_name'))
                            <span class="badge badge-danger" role="alert">
                                <strong>{{ $errors->first('order_name') }}</strong>
                            </span>

                            @endif
                        </div>
						</div>
						<div class="form-group row">
						    <label for="order_email" class="col-sm-4">Email</label>
						    <div class="col-sm-8">
						      <input type="email" name="order_email" class="form-control" id="order_email">
						    
						   @if ($errors->has('order_email'))
                            <span class="badge badge-danger" role="alert">
                                <strong>{{ $errors->first('order_email') }}</strong>
                            </span>
                            @endif
                        </div>
						</div>
						<div class="form-group row">
						    <label for="order_phone" class="col-sm-4">Phone</label>
						    <div class="col-sm-8">
						      <input type="text" name="order_phone" class="form-control" id="order_phone">
						    
						     @if ($errors->has('order_phone'))
                            <span class="badge badge-danger" role="alert">
                                <strong>{{ $errors->first('order_phone') }}</strong>
                            </span>
                            @endif
                             </div>
						</div>
						<div class="form-group row">
						    <label for="order_message" class="col-sm-4">Message</label>
						    <div class="col-sm-8">
						      <textarea name="order_message" cols="4" rows="5" value="{{old('order_message')}}" class="form-control" id="order_message">{{ old('order_message')}}</textarea>
				                  
						    
						     @if ($errors->has('order_message'))
                            <span class="badge badge-danger" role="alert">
                                <strong>{{ $errors->first('order_message') }}</strong>
                            </span>
                            @endif
                            </div>
						</div>

						<div class="form-group row">
	                        <label class="col-sm-4" for="payments">Payment Method</label>
	                        <div class="col-sm-8">
	                            
                                    <select name="payment_id" class="form-control" value="{{old('payment_id')}}" id="payments">
										<option value="0" disabled="true" selected="true" >===Select Payment Method===</option>
										@foreach($payments as $payment))
										<option value="{{$payment->short_name}}" ng-model="payment_id">{{$payment->name}}</option>
										@endforeach
									</select>
									 @if ($errors->has('payment_id'))
                            <span class="badge badge-danger" role="alert">
                                <strong>{{ $errors->first('payment_id') }}</strong>
                            </span>
                            @endif

                                    
	                        </div>
	                    </div>

							@foreach(App\Models\Payment::orderBy('id','desc')->get(); as $payment)
							
							@if($payment->short_name=="Cash")
							<div id="payment_{{ $payment->short_name}}" class="d-none alert alert-success" >
								<div class="cash">
										<img src="{{asset('images/payments/'.$payment->image)}}" height="160px" width="100%" alt="Rocket Image" class="">
									
									<hr>
									<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4><br>
									<h5 class="alert alert-info">For Cash in there is noting necssary.Just Finish Your Booking</h5><br>
								</div>
							</div>
							@else
							<div id="payment_{{ $payment->short_name}}" class="d-none alert alert-success">
								<div class="bkash">
									
										<img src="{{asset('images/payments/'.$payment->image)}}"  height="160px" width="100%" alt="Rocket Image" class="">
									
								
									<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4>
									<p>
										<strong>{{$payment->name}} No: {{ $payment->no}}</strong><br>
										<strong class="badge badge-info"> Account Type: {{ $payment->type}}</strong>
										<div class="alert alert-danger">
											<p>
											Please send the Above money to this <span badge badge-success>{{$payment->name}}</span> money and write your transaction code bellow  there
											</p>
										</div>
									</p>
								</div>
							</div>
							@endif
							
							@endforeach
							<input type="text" id="transaction_id" placeholder="Please inter Transaction ID" name="transaction_id" class="form-control d-none">
							
						<hr>
						
						<button type="sumbit" class="btn btn-primary float-right" >Booking</button>
						</form>
				</div>
				<div class="col-sm-5">
					<div class="card">
						<div class="card-header">Schedule Information</div>
						<div class="card-body">
							<table class="table table-striped">
								<tbody>
									<tr>
										<th>Medical Name</th>
										<td>{{ $schedule->medical->medical_name }} </td>
									</tr>
									<tr>
										<th>Doctor Name</th>
										<td>{{ $schedule->doctor->doctor_first_name }} {{ $schedule->doctor->doctor_last_name }}</td>
									</tr>
									<tr>
										<th>Floor</th>
										<td>{{ $schedule->floor->floor_name }}st Floor </td>
									</tr>

									<tr>
										<th>Room No</th>
										<td>{{ $schedule->room->room_number }} </td>
									</tr>

									<tr>
										<th>Day</th>
										<td>{{ $schedule->schedule_day }} </td>
									</tr>
									<tr>
										<th>Start</th>
										<td>{{ $schedule->start_from }} </td>
									</tr>
									<tr>
										<th>End</th>
										<td>{{ $schedule->end_to }} </td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		

@endsection

@section('scripts')

<script type="text/javascript">
	$("#payments").change(function() {
		//alert('dsfaskf');
		$payment_method=$("#payments").val();
		if ($payment_method=="Cash") {
			$("#payment_Cash").removeClass('d-none');
			$("#payment_Rocket").addClass('d-none');
			$("#transaction_id").addClass('d-none');
			$("#payment_bKash").addClass('d-none');
		}else if($payment_method=="bKash"){
			$("#payment_bKash").removeClass('d-none');
			$("#transaction_id").removeClass('d-none');
			$("#payment_Cash").addClass('d-none');
			$("#payment_Rocket").addClass('d-none');
		}else if($payment_method=="Rocket"){
			$("#payment_Rocket").removeClass('d-none');
			$("#transaction_id").removeClass('d-none');
			$("#payment_Cash").addClass('d-none');
			$("#payment_bKash").addClass('d-none');
		}
	});
</script>


  <script>
        $(function() {
            setTimeout(function() {
                $('#Message').delay(1000).fadeOut('slow');
            }, 1000);
        });
    </script>

@endsection