@extends('frontend.layouts.master')
@section('title')
    Home
@endsection
@section('content')

	           <div class="row  justify-content-center" style="position: relative;padding-top: 20px;">
				<div class="col-lg-8 style-design">
					<form method="post" action="{{ route('search_schedule') }}">
						{{ csrf_field()}}
		                <div class="form-group row">
						    <h4 for="doctor_id" class="col-sm-6">Select Doctor appointment</h4>
						    <div class="col-sm-6">
						      <select name="doctor_id" id="doctor_id" class="form-control">
					                          <option value="0" disabled="true" selected="true">===Select Doctor===</option>
					                          @foreach($doctors as $doctor)
					                          <option value="{{$doctor->id}}" >{{$doctor->doctor_first_name}} {{ $doctor->doctor_last_name}}</option>
					                          @endforeach
					                  </select>
						    </div>
						    
						</div>
						<button type="sumbit" class="btn btn-primary float-right">Search Doctor Schedule</button>

						</form>
					</div>
<div class="col-lg-12 welcome-tanvir">
						@if(!(empty($doctor_info)))
						<div class="row" style="background-color: #effeef;position: relative;padding-top: 20px;padding-bottom: 20px">
							<div class="col-sm-8">
								<div class="card-body" style="margin-top: 8px">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Doctor Name : </th>
												<td>{{ $doctor_info->doctor_first_name}} {{ $doctor_info->doctor_last_name}}</td>
											</tr>

											<tr>
												<th>Which Types of Doctor : </th>
												<td>{{ $doctor_info->category->category_name}} </td>
											</tr>


											<tr>
												<th>Doctor Email : </th>
												<td>{{ $doctor_info->doctor_email}}</td>
											</tr>
											<tr>
												<th>Doctor Gender </th>
												<td>{{ $doctor_info->doctor_gender}} </td>
											</tr>
											
											<tr>
												<th>Doctor Contact : </th>
												<td>{{ $doctor_info->doctor_contact_no}} </td>
											</tr>
											<tr>
												<th>Doctor Education : </th>
												<td>{{ $doctor_info->doctor_education}} </td>
											</tr>
											<tr>
												<th>Doctor Designation : </th>
												<td>{{ $doctor_info->doctor_designation}} </td>
											</tr>
											<tr>
												<th>Doctor Address : </th>
												<td>{{ $doctor_info->doctor_address}} </td>
											</tr>
											
										</thead>
									</table>
								</div>
							</div>
							<div class="col-sm-4 card"">
								 <img  src="{{asset('backend/admin/images/doctors/'.$doctor_info->doctor_image)}}" class="img-circle" height="300px" width="100%">
							</div>
						</div>
						@else

						@endif
				</div>
				@if(!(empty($schedule_all)))
				<div class="col-lg-12" style="position: relative;padding-top: 20px">
					<div class="table-responsive mt-2">
				        <table id="schedules" class="table table-bordered table-striped">
				          <caption>List of schedules</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Doctor Name</th>
				  						<th>Medical Name</th>
				  						<th>Floor</th>
				  						<th>Room No</th>
				  						<th>Day</th>
				  						<th>Start Time</th>
				  						<th>End Time</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<div style="display: none;">{{$a=1}}</div>
				  					@foreach($schedule_all as $schedule)
				  					<tr>
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $schedule->doctor->doctor_first_name}} {{ $schedule->doctor->doctor_last_name}}</td>
				  						<td class="text-center">{{ $schedule->medical->medical_name}}</td>
				  						<td class="text-center">{{ $schedule->floor->floor_name}}</td>
				  						<td class="text-center">{{ $schedule->room->room_number}}</td>
				  						<td class="text-center">{{ $schedule->schedule_day}}</td>
				  						<td class="text-center">{{ $schedule->start_from}}</td>
				  						<td class="text-center">{{ $schedule->end_to}}</td>
				             
				            
				  						<td class="text-center"> 
				  							<a href="{{route('request_to_booking', $schedule->id)}}" class="btn btn-primary btn-sm">Booking Now</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
				</div>
				@else

				@endif

				</div>
			
@endsection