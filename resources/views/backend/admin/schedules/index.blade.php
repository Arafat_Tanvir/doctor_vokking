@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<strong class="pull-left" >
			              <h4 style="color: green;font-size: 28px;font-style: italic;"> All Schedule Show </h1>
			            </strong>
			            <span class="pull-right">
			              <a href="{{ route('schedule-admin-create')}}" class="btn btn-primary">Create Schedule</a>
			            </span>
					</div>
					<div class="box-body">
				      <div class="table-responsive mt-2">
				        <table id="schedules" class="table table-bordered table-striped">
				          <caption>List of schedules</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Medical Name</th>
				  						<th>Doctor Name</th>
				  						<th>Floor Name </th>
				  						<th>Room No</th>
				  						<th></th>
				  						<th>schedule Designation</th>
				  						<th>schedule Address</th>
				  						
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($schedules as $schedule)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $schedule->medical->medical_name}}</td>
				  						<td class="text-center">{{ $schedule->doctor->doctor_first_name }}</td>
				  						<td class="text-center">{{ $schedule->floor->floor_name}}</td>
				  						<td class="text-center">{{ $schedule->room->room_number}}</td>
				  						<td class="text-center">{{ $schedule->schedule_day}}</td>
				  						<td class="text-center">{{ $schedule->start_from}}</td>
				  						<td class="text-center">{{ $schedule->end_to}}</td>
				  						
				             
				            
				  						<td class="text-center"> <!-- <a href="{{route('schedule-admin-show', $schedule->id)}}" class="btn btn-primary btn-sm">Show</a> -->
				  							<a href="{{route('schedule-admin-edit', $schedule->id)}}" class="btn btn-warning btn-sm">Edit</a>
				                <a href="#DeleteModal{{ $schedule->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$schedule->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('schedule-admin-delete', $schedule->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
				   </div>
			  </div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

<script>
	$(document).ready(function() {
	    $('#schedules').DataTable();
	} );
</script>
@endsection


