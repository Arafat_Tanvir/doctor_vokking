@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-10 col-sm-offset-1" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4>Doctor Create</h1>
			            </div>
					</div>
					<div class="box-body">
					      <form action="{{ route('schedule-admin-update',$schedule->id) }}" class="user" method="post" enctype="multipart/form-data">
					         <input type="hidden" name="_token" value="{{ csrf_token() }}">
					        

					        <div class="form-group">
					              <label for="doctor_id">Select Doctor</label>
					              <div class="form-input">
					                  <select name="doctor_id" id="doctor_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select Doctor===</option>
					                          @foreach($doctors as $doctor)
					                          <option value="{{$doctor->id}}" {{ $schedule->doctor_id == $doctor->id ? 'selected' : '' }}>{{$doctor->doctor_first_name}} {{ $doctor->doctor_last_name}}</option>
					                          @endforeach
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_id')) ? $errors->first('doctor_id') : ''}}
					                  </div>
					              </div>
					          </div>


				           <div class="form-group">
				              <label for="schedule_day">Day</label>
				              <div class="form-input">
				                  <select name="schedule_day" id="schedule_day" class="form-control form-control-user is-valid form-control-sm input-md" value="">
				                          <option value="0" disabled="true" selected="true">===Select Schedule Day===</option>
				                          <option value="Saturday" {{ $schedule->schedule_day =="Saturday" ? 'selected' : '' }}>Saturday</option>
				                          <option value="Sunday" {{ $schedule->schedule_day == "Sunday" ? 'selected' : '' }}>Sunday</option>
				                          <option value="Monday" {{ $schedule->schedule_day == "Monday" ? 'selected' : '' }}>Monday</option>
				                          <option value="Tuesday" {{ $schedule->schedule_day == "Tuesday" ? 'selected' : '' }}>Tuesday</option>
				                          <option value="Wednesday" {{ $schedule->schedule_day == "Wednesday" ? 'selected' : '' }}>Wednesday</option>
				                          <option value="Thursday" {{ $schedule->schedule_day == "Thursday" ? 'selected' : '' }}>Thursday</option>
				                          <option value="Friday" {{ $schedule->schedule_day == "Friday" ? 'selected' : '' }}>Friday</option>
				                  </select>
				                  <div class="valid-feedback">
				                    {{ ($errors->has('schedule_day')) ? $errors->first('schedule_day') : ''}}
				                  </div>
				              </div>
				          </div>

					           <div class="form-group">
		                            <label for="start_from">From</label>
		                           <div class="form-input">
		                                <input type="text" class="time-pickerfrom form-control" value="{{ $schedule->start_from}}" name="start_from" id="start_from">
		                            </div>
		                            <div class="valid-feedback">
					                    {{ ($errors->has('start_from')) ? $errors->first('start_from') : ''}}
					                  </div>
		                        </div>

		                        <div class="form-group clockpickerTo">
		                            <label for="end_to">To</label>
		                            <div class="form-input">
		                                <input type="text" class="time-pickerto form-control" value="{{ $schedule->end_to}}" name="end_to" id="end_to">
		                            </div>
		                            <div class="valid-feedback">
					                    {{ ($errors->has('end_to')) ? $errors->first('end_to') : ''}}
					                  </div>
		                        </div>

					           <div class="form-group">
					              <label for="medical_id">Hospital/Medical Name </label>
					              <div class="form-input">
					                  <select name="medical_id" id="medical_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select Medical===</option>
					                          @foreach($medicals as $medical)
					                          <option value="{{$medical->id}}" {{ $schedule->medical_id == $medical->id ? 'selected' : ''}}>{{$medical->medical_name}}</option>
					                          @endforeach
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('medical_id')) ? $errors->first('medical_id') : ''}}
					                  </div>
					              </div>
					          </div>

					        

					       <div class="form-group">
		                        <label for="floor_id">{{ __('Choose Floor') }}</label>
		                        <div class="form-input">
		                            <select name="floor_id" type="text" id="floor_id" class="form-control{{ $errors->has('floor_id') ? ' is-valid' : '' }}" value="{{ old('floor_id') }}">
		                                <option value="0" disabled="true" selected="true">===Select Floor===</option>
		                                @foreach(App\Models\Floor::orderBy('id','desc')->get(); as $floor)
					                        <option value="{{$floor->id}}" {{ $floor->id ==$schedule->floor_id ? 'selected' : ''}}>{{$floor->floor_name}}</option>
				                        @endforeach
		                            </select>
		                            @if ($errors->has('floor_id'))
		                            <span class="btn-danger" role="alert">
		                                <strong>{{ $errors->first('floor_id') }}</strong>
		                            </span>
		                            @endif
		                        </div>
		                    </div>


					         <div class="form-group">
		                        <label for="room_id">{{ __('Choose Room') }}</label>
		                        <div class="form-input row">
		                        	<div class="col-sm-6">
		                        		<select name="room_id" type="text" id="room_id" class="form-control{{ $errors->has('room_id') ? ' is-valid' : '' }}" value="{{ old('room_id') }}">
		                                <option value="0" disabled="true" selected="true">===Select Room===</option>
		                                @foreach(App\Models\Room::orderBy('id','desc')->get(); as $room)
					                        <option value="{{$room->id}}" {{ $room->id ==$schedule->room_id ? 'selected' : ''}}>{{$room->room_number}}</option>
				                        @endforeach
		                            </select>
		                        	</div>
		                            
		                           
		                            	<div class="col-sm-6">
		                        		<select name="sub_room_id" type="text" id="sub_room_id" class="form-control" value="{{ old('sub_room_id') }}">
		                                <option value="0" disabled="true" selected="true">===Select Room===</option>
		                            </select>
		                        	
		                            </div>
		                            @if ($errors->has('room_id'))
		                            <span class="btn-danger" role="alert">
		                                <strong>{{ $errors->first('room_id') }}</strong>
		                            </span>
		                            @endif
		                        </div>
		                    </div>


				          

					          <button class="btn btn-primary" type="submit">Create Schedule</button>
					      </form>
					</div>
			  </div>
			</div>
		</div>
	</div>
@endsection


@section('scripts')
<script src="{{ asset('/' )}}backend/admin/timePicker/jquery-timepicker.js"></script>
<script src="{{ asset('/' )}}backend/admin/timePicker/timepicker.js"></script>

 <script>
    $().ready(function(e) {
        $(".time-pickerto").hunterTimePicker();
        $('.time-pickerfrom').hunterTimePicker();
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){
		$('#medical_id').on('change',function(){
			var medical_id=$(this).val();
			if(medical_id)
			{
				$.ajax({
				url:'{{ url('')}}/floors/ajax/'+medical_id,
				type:"GET",
				dataType:"json",
					success:function(data)
					{
						$('#floor_id').empty();
						$("#floor_id").append('<option disabled selected value="0">===Select Floor====</option>');
						$.each(data,function(key,value){
							$('#floor_id').append('<option value="'+key+'">'+value+'</option>')
						});
					}
			   });
			}else
			{
				$('#floor_id').empty();
			}
		});
		$('#floor_id').on('change',function(){
			var floor_id=$(this).val();
			if(floor_id)
			{
				$.ajax({
				url:'{{ url('')}}/rooms/ajax/'+floor_id,
				type:"GET",
				dataType:"json",
					success:function(data)
					{
						$('#room_id').empty();
						$("#room_id").append('<option disabled selected value="0">===Select Room====</option>');
						$.each(data,function(key,value){
							$('#room_id').append('<option value="'+key+'">'+value+'</option>')
						});
					}
			   });
			}else
			{
				$('#room_id').empty();
			}
		});
		$('#room_id').on('change',function(){
			$('#sub_room_id').hide();
			var room_id=$(this).val();
			if(room_id)
			{
				$.ajax({
				url:'{{ url('')}}/rooms_sub_ajax/ajax/'+room_id,
				type:"GET",
				dataType:"json",
				success:function(data1)
					{
						if (Object.keys(data1).length>0) {
							$('#sub_room_id').empty();
							$('#sub_room_id').show();
							$("#sub_room_id").append('<option disabled selected value="0">===Select Sub Room====</option>');
							$.each(data1,function(key1,value1){
								$('#sub_room_id').append('<option  value="'+key1+'">'+value1+'</option>')
						    });
						}else{
							$('#sub_room_id').hide();
						}
					}
			   });
			}else
			{
				$('#room_id').empty();
			}
		});
	});
</script>
@endsection
