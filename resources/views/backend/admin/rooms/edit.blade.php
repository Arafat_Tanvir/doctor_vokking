@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-10 col-sm-offset-1" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4>Doctor Create</h1>
			            </div>
					</div>
					<div class="box-body">
					      <form action="{{ route('rooms_admin_update',$room->id) }}" class="user" method="post" >
					         <input type="hidden" name="_token" value="{{ csrf_token() }}">

					         <div class="form-group">
					              <label for="medical_id">Medical Name</label>
					              <div class="form-input">
					                  <select name="medical_id" id="medical_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select medical Name===</option>
					                          @foreach($medicals as $medical)
					                          <option value="{{$medical->id}}" {{ $room->floor->medical_id == $medical->id ? 'selected' : '' }}>{{$medical->medical_name}}</option>
					                          @endforeach
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('medical_id')) ? $errors->first('medical_id') : ''}}
					                  </div>
					              </div>
					          </div>
					        
					          <div class="form-group">
		                        <label for="floor_id">{{ __('Choose Floor') }}</label>
		                        <div class="form-input">
		                            <select name="floor_id" type="text" id="floor_id" class="form-control">
		                                <option value="0" disabled="true" selected="true">===Select Floor===</option>
		                                 @foreach(App\Models\Floor::orderBy('id','desc')->get(); as $floor)
					                        <option value="{{$floor->id}}" {{ $floor->id ==$room->floor_id ? 'selected' : ''}}>{{$floor->floor_name}}</option>
				                        @endforeach
		                            </select>


		                            @if ($errors->has('floor_id'))
		                            <span class="alart alart-denger" role="alert">
		                                <strong>{{ $errors->first('floor_id') }}</strong>
		                            </span>
		                            @endif
		                        </div>
		                    </div>

					       <div class="form-group">
				              <label for="room_number">Room Number</label>
				              <div class="form-input">
				                  <input type="number" class="form-control form-control-user is-valid form-control-sm" name="room_number" id="room_number" placeholder="Enter Room Number" value="{{ $room->room_number}}" >
				                  <div class="valid-feedback">
				                    {{ ($errors->has('room_number')) ? $errors->first('room_number') : ''}}
				                  </div>
				              </div>
				          </div>
				          <div class="text-right">
					          <button class="btn btn-primary" type="submit">Update Room</button>
					          </div>
					      </form>
					</div>
			  </div>
			</div>
		</div>
	</div>
@endsection
          
@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		$('#medical_id').on('change',function(){
			var medical_id=$(this).val();
			if(medical_id)
			{
				$.ajax({
				url:'{{ url('')}}/floors/ajax/'+medical_id,
				type:"GET",
				dataType:"json",
					success:function(data)
					{
						$('#floor_id').empty();
						$("#floor_id").append('<option disabled selected value="0">===Select Floor====</option>');
						$.each(data,function(key,value){
							$('#floor_id').append('<option value="'+key+'">'+value+'</option>')
						});
					}
			   });
			}else
			{
				$('#floor_id').empty();
			}
		});
	});
</script>
@endsection
