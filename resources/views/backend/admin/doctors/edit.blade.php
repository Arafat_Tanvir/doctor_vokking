@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-8" style="margin-top: 12px;margin-bottom: 12px">
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4>Update Doctor Information</h1>
			            </div>
					</div>
					<div class="box-body">
					          <form method="post" action="{{ route('doctor-admin-update',$doctor->id) }}" class="user" enctype="multipart/form-data">
 					    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					          <div class="form-group">
					              <label for="doctor_first_name">Doctor First Name</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_first_name" id="doctor_first_name" placeholder="Enter Doctor First Name" value="{{ $doctor->doctor_first_name }}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_first_name')) ? $errors->first('doctor_first_name') : ''}}
					                  </div>
					              </div>
					          </div>
					          <div class="form-group">
					              <label for="doctor_last_name">Doctor Last Name</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_last_name" id="doctor_last_name" placeholder="Enter DoctorLast Name" value="{{ $doctor->doctor_last_name }}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_last_name')) ? $errors->first('doctor_last_name') : ''}}
					                  </div>
					              </div>
					          </div>

					          <div class="form-group">
					              <label for="doctor_email">Doctor Email</label>
					              <div class="form-input">
					                  <input type="email" class="form-control form-control-user is-valid form-control-sm" name="doctor_email" id="doctor_email" placeholder="Enter Doctor Email" value="{{ $doctor->doctor_email}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_email')) ? $errors->first('doctor_email') : ''}}
					                  </div>
					              </div>
					          </div>

					          <div class="form-group">
					              <label for="doctor_contact_no">Doctor Contact No</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_contact_no" id="doctor_contact_no" placeholder="Enter Contact No" value="{{ $doctor->doctor_contact_no}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_contact_no')) ? $errors->first('doctor_contact_no') : ''}}
					                  </div>
					              </div>
					          </div>
					          {{ $doctor->doctor_gender}}
					         <div class="form-group">
					              <label for="doctor_gender">Doctor Gender</label>
					              <div class="form-input row">
					              	<div class="col-sm-4 text-center">
					              		 <input type="radio" name="doctor_gender"  value="male" {{ $doctor->doctor_gender == 'male' ? 'checked' : '' }} >Male
					              	</div>
					              	<div class="col-sm-4 text-center">
					              		 <input type="radio" name="doctor_gender" value="female" {{ $doctor->doctor_gender == 'female' ? 'checked' : '' }} >Female
					              	</div>
					              	<div class="col-sm-4 text-center">
					              		 <input type="radio" name="doctor_gender" value="other" @if($doctor->doctor_gender == 'other') checked='checked' @endif >Other
					              	</div>

					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_gender')) ? $errors->first('doctor_gender') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="doctor_designation">Doctor Designation</label>
					              <div class="form-input">
					                  <select name="doctor_designation" id="doctor_designation" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select Doctor Designation===</option>
					                       
					                          <option value="Allergists/Immunologists" {{ $doctor->doctor_designation == "Allergists/Immunologists" ? 'selected' : '' }}>Allergists/Immunologists</option>
					                          <option value="Anesthesiologists" {{ $doctor->doctor_designation == "Anesthesiologists" ? 'selected' : '' }}>Anesthesiologists</option>
					                          <option value="Cardiologists" {{ $doctor->doctor_designation == "Cardiologists" ? 'selected' : '' }}>Cardiologists</option>
					                          <option value="Colon and Rectal Surgeons" {{ $doctor->doctor_designation == "Colon and Rectal Surgeons" ? 'selected' : '' }}>Colon and Rectal Surgeons</option>
					                          <option value="Critical Care Medicine Specialists" {{ $doctor->doctor_designation == "Critical Care Medicine Specialists" ? 'selected' : '' }}>Critical Care Medicine Specialists</option>
					                          <option value="Dermatologists" {{ $doctor->doctor_designation == "Dermatologists" ? 'selected' : '' }}>Dermatologists</option>
					                          <option value="Endocrinologists" {{ $doctor->doctor_designation == "Endocrinologists" ? 'selected' : '' }}>Endocrinologists</option>
					                          <option value="Emergency Medicine Specialists" {{ $doctor->doctor_designation == "Emergency Medicine Specialists" ? 'selected' : '' }}>Emergency Medicine Specialists</option>
					                          <option value="Gastroenterologists" {{ $doctor->doctor_designation == "Gastroenterologists" ? 'selected' : '' }}>Gastroenterologists</option>
					                          <option value="Hematologists" {{ $doctor->doctor_designation == "Hematologists" ? 'selected' : '' }}>Hematologists</option>
					                         
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_designation')) ? $errors->first('doctor_designation') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="doctor_education">Doctor Education</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_education" id="doctor_education" placeholder="Enter Category name" value="{{ $doctor->doctor_education }}">
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_education')) ? $errors->first('doctor_education') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="category_id">Types Of Doctor</label>
					              <div class="form-input">
					                  <select name="category_id" id="category_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select Type Of  Doctor===</option>
					                          @foreach($categories as $category)
					                          <option value="{{$category->id}}" {{ $doctor->category_id == $category->id ? 'selected' : '' }}>{{$category->category_name}}</option>
					                          @endforeach
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('category_id')) ? $errors->first('category_id') : ''}}
					                  </div>
					              </div>
					          </div>

					          <div class="form-group">
					              <label for="doctor_address">Doctor Address</label>
					              <div class="form-input">
					                  <textarea name="doctor_address" cols="4" rows="5" value="{{old('doctor_address')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="doctor_address" required>{{ $doctor->doctor_address }}</textarea>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_address')) ? $errors->first('doctor_address') : ''}}
					                  </div>
					              </div>
					          </div>

					        <div class="form-group">
					            <label for="doctor_image">Doctor Image</label>
					              <div class="form-input">
					                  <input type="file" class="form-control form-control-user is-valid form-control-sm" name="doctor_image" id="doctor_image" placeholder="Enter Category name" value="{{old('doctor_image')}}" >
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_image')) ? $errors->first('doctor_image') : ''}}
					                  </div>
					              </div>
					          </div>
					          <button class="btn btn-primary" type="submit">Update Doctor</button>
					      </form>
					</div>
			  </div>
			</div>
			<div class="col-sm-4" style="margin-top: 250px;margin-bottom: 12px">
				<div class="text-center">
					 <img  src="{{asset('backend/admin/images/doctors/'.$doctor->doctor_image)}}" class="" height="300px" width="100%">
				</div>
			</div>
		</div>
	</div>
@endsection
