@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-10 col-sm-offset-1" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4>Doctor Create</h1>
			            </div>
					</div>
					<div class="box-body">
					      <form action="{{ route('doctor-admin-store') }}" class="user" method="post" enctype="multipart/form-data">
					         {{ csrf_field()}}
					        
					          <div class="form-group">
					              <label for="doctor_first_name">Doctor First Name</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_first_name" id="doctor_first_name" placeholder="Enter Doctor First Name" value="{{old('doctor_first_name')}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_first_name')) ? $errors->first('doctor_first_name') : ''}}
					                  </div>
					              </div>
					          </div>
					          <div class="form-group">
					              <label for="doctor_last_name">Doctor Last Name</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_last_name" id="doctor_last_name" placeholder="Enter DoctorLast Name" value="{{old('doctor_last_name')}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_last_name')) ? $errors->first('doctor_last_name') : ''}}
					                  </div>
					              </div>
					          </div>

					          <div class="form-group">
					              <label for="doctor_email">Doctor Email</label>
					              <div class="form-input">
					                  <input type="email" class="form-control form-control-user is-valid form-control-sm" name="doctor_email" id="doctor_email" placeholder="Enter Doctor Email" value="{{old('doctor_email')}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_email')) ? $errors->first('doctor_email') : ''}}
					                  </div>
					              </div>
					          </div>

					          <div class="form-group">
					              <label for="doctor_contact_no">Doctor Contact No</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_contact_no" id="doctor_contact_no" placeholder="Enter Contact No" value="{{old('doctor_contact_no')}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_contact_no')) ? $errors->first('doctor_contact_no') : ''}}
					                  </div>
					              </div>
					          </div>

					         <div class="form-group">
					              <label for="doctor_gender">Doctor Gender</label>
					              <div class="form-input row">
					              	<div class="col-sm-4 text-center">
					              		 <input type="radio" name="doctor_gender" value="male">Male
					              	</div>
					              	<div class="col-sm-4 text-center">
					              		 <input type="radio" name="doctor_gender" value="female">Female
					              	</div>
					              	<div class="col-sm-4 text-center">
					              		 <input type="radio" name="doctor_gender" value="other" >Other
					              	</div>

					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_gender')) ? $errors->first('doctor_gender') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="doctor_designation">Doctor Designation</label>
					              <div class="form-input">
					                  <select name="doctor_designation" id="doctor_designation" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select Doctor Designation===</option>
					                       
					                          <option value="Allergists/Immunologists">Allergists/Immunologists</option>
					                          <option value="Anesthesiologists">Anesthesiologists</option>
					                          <option value="Cardiologists">Cardiologists</option>
					                          <option value="Colon and Rectal Surgeons">Colon and Rectal Surgeons</option>
					                          <option value="Critical Care Medicine Specialists">Critical Care Medicine Specialists</option>
					                          <option value="Dermatologists">Dermatologists</option>
					                          <option value="Endocrinologists">Endocrinologists</option>
					                          <option value="Emergency Medicine Specialists">Emergency Medicine Specialists</option>
					                          <option value="Gastroenterologists">Gastroenterologists</option>
					                          <option value="Hematologists">Hematologists</option>
					                         
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_designation')) ? $errors->first('doctor_designation') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="doctor_education">Doctor Education</label>
					              <div class="form-input">
					                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="doctor_education" id="doctor_education" placeholder="Enter Category name" value="{{old('doctor_education')}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_education')) ? $errors->first('doctor_education') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="category_id">Types Of Doctor</label>
					              <div class="form-input">
					                  <select name="category_id" id="category_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select Type Of  Doctor===</option>
					                          @foreach($categories as $category)
					                          <option value="{{$category->id}}">{{$category->category_name}}</option>
					                          @endforeach
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('category_id')) ? $errors->first('category_id') : ''}}
					                  </div>
					              </div>
					          </div>

					          <div class="form-group">
					              <label for="doctor_address">Doctor Address</label>
					              <div class="form-input">
					                  <textarea name="doctor_address" cols="4" rows="5" value="{{old('doctor_address')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="doctor_address" required>{{ old('doctor_address')}}</textarea>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_address')) ? $errors->first('doctor_address') : ''}}
					                  </div>
					              </div>
					          </div>

					        <div class="form-group">
					            <label for="doctor_image">Doctor Image</label>
					              <div class="form-input">
					                  <input type="file" class="form-control form-control-user is-valid form-control-sm" name="doctor_image" id="doctor_image" placeholder="Enter Category name" value="{{old('doctor_image')}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('doctor_image')) ? $errors->first('doctor_image') : ''}}
					                  </div>
					              </div>
					          </div>
					          <button class="btn btn-primary" type="submit">Create Doctor</button>
					      </form>
					</div>
			  </div>
			</div>
		</div>
	</div>
@endsection
