@extends('Backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row " style="margin-top: 12px;margin-bottom: 12px">
	    <div class="col-sm-12">
		    <div class="box">
				<div class="box-header">
					<div class="text-left">
		              <h4> All Category Show </h4>
		            </div>
				</div>
				<div class="box-body row">
					<div class="col-sm-8">
			            <table class="table">
		                    <tr>
		                        <th>Category Name</th>
		                        <th>Doctor Name</th>
		                        <th>Doctor Email</th> 
		                        <th>Doctor Contact No</th> 
		                        <th>Doctor Education</th> 
		                        <th>Doctor Designation</th> 
		                        <th>Doctor Address</th> 
		                    </tr>
		                    <tr>
		                        <td>{{$doctor->category->category_name}} </td>
		                        <td>{{$doctor->doctor_first_name }}{{ $doctor->doctor_last_name}}</td>
		                        <td>{{$doctor->doctor_email }}</td>
		                        <td>{{$doctor->doctor_contact_no }}</td>
		                        <td>{{$doctor->doctor_education }}</td>
		                        <td>{{$doctor->doctor_designation }}</td>
		                        <td>{{$doctor->doctor_address }}</td>

		                    </tr>
		                </table>
	                </div>
	                 <div class="col-ms-4 text-center">
		        	@if($doctor->doctor_image)
		                <img  src="{{asset('backend/admin/images/doctors/'.$doctor->doctor_image)}}" height="300px" width="200px">
		            @else
		                <p>N/A</p>
		            @endif
		        </div>
		    </div>
		   </div>
        </div>
       
    </div>
</div>
@endsection
