@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<strong class="pull-left" >
			              <h4 style="color: green;font-size: 28px;font-style: italic;"> All Doctor Show </h1>
			            </strong>
			            <span class="pull-right">
			              <a href="{{ route('doctor-admin-create')}}" class="btn btn-primary">Create Doctor</a>
			            </span>
					</div>
					<div class="box-body">
				      <div class="table-responsive mt-2">
				        <table id="doctors" class="table table-bordered table-striped">
				          <caption>List of doctors</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Doctor Name</th>
				  						<th>Doctor Email</th>
				  						<th>Doctor Image</th>
				  						<th>Doctor Gender</th>
				  						<th>Doctor Education</th>
				  						<th>Doctor Designation</th>
				  						<th>Doctor Address</th>
				  						<th>Doctor Category</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($doctors as $doctor)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $doctor->doctor_first_name }} {{ $doctor->doctor_last_name}}</td>
				  						<td class="text-center">{{ $doctor->doctor_email }}</td>
				  						<td class="text-center">
				  							@if($doctor->doctor_image)
							                  <p>
							                    <img  src="{{asset('backend/admin/images/doctors/'.$doctor->doctor_image)}}" class="img-circle" height="100px" width="100px">
							                  </p>
							                  @else
							                    <p>N/A</p>
							                  @endif
				  						</td>
				  						<td class="text-center">{{ $doctor->doctor_gender }}</td>
				  						<td class="text-center">{{ $doctor->doctor_education }}</td>
				  						<td class="text-center">{{ $doctor->doctor_designation }}</td>
				  						<td class="text-center">{{ $doctor->category->category_name }}</td>
				  						<td class="text-center">
				  						    @if($doctor->doctor_address)
				                  <p>{{ $doctor->doctor_address}}</p>
				                  @else
				                    <p>N/A</p>
				                  @endif
				  						</td>
				             
				            
				  						<td class="text-center"> <a href="{{route('doctor-admin-show', $doctor->id)}}" class="btn btn-primary btn-sm">Show</a>
				  							<a href="{{route('doctor-admin-edit', $doctor->id)}}" class="btn btn-warning btn-sm">Edit</a>
				                <a href="#DeleteModal{{ $doctor->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$doctor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('doctor-admin-delete', $doctor->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
				   </div>
			  </div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	$(document).ready(function() {
	    $('#doctors').DataTable();
	} );
</script>
@endsection