@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4> All order Show </h1>
			            </div>
					</div>
					<div class="box-body">
				      <div class="table-responsive mt-2">
				        <table id="orders" class="table table-bordered table-striped">
				          <caption>List of orders</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Doctor Name</th>
				  						<th>Patient Name</th>
				  						<th>Medical</th>
				  						<th>Day</th>
				  						<th>Status</th>
				  						<th>order Education</th>
				  						<th>order Designation</th>
				  						
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($orders as $order)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $order->doctor->doctor_first_name }} {{ $order->doctor->doctor_last_name}}</td>
				  						<td class="text-center">{{ $order->order_name }}</td>
				  						<td class="text-center">{{ $order->schedule->medical->medical_name }}</td>
				  						<td class="text-center">{{ $order->schedule->schedule_day }}</td>
				  						<td class="text-center">
				  							@if($order->is_confirmed)
												<button  class="float-right btn btn-success btn-sm">Complete</button>
											@else
												<button class="float-right btn btn-danger btn-sm">UnComplete</button>
											@endif
				  						</td>
				  						<td class="text-center">{{ $order->schedule->start_from }}</td>
				  						<td class="text-center">{{ $order->schedule->end_to}}</td>
				  						<td class="text-center"> <a href="{{route('order-admin-show', $order->id)}}" class="btn btn-primary btn-sm">Show</a>
				  							<!-- <a href="{{route('order-admin-edit', $order->id)}}" class="btn btn-warning btn-sm">Edit</a> -->
				                <a href="#DeleteModal{{ $order->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('order-admin-delete', $order->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
				   </div>
			  </div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

<script>
	$(document).ready(function() {
	    $('#orders').DataTable();
	} );
</script>
@endsection
