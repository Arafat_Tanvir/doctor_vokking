@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header" style="background-color: #eefffe">
						<div class="row">
			              <div class="col-sm-6" >
			              	<h4 class="text-center">Patient Information </h4>
			              	<div class="text-center" style="margin-top: 110px;margin-bottom: 12px">
			              		<table id="orders" class="table">
						  				<tbody>
						  					<tr>
						  						<th>Patient Name</th>
						  						<td class="text-center">{{ $order->order_name }}</td>
						  					</tr>
						  					<tr>
						  						<th>Contact name</th>
						  						<td class="text-center">{{ $order->order_phone}}</td>
						  					</tr>
						  					<tr>
						  						<th>Message</th>
						  						<td class="text-center">{{ $order->order_message }}</td>
						  					</tr>
						  					<tr>
						  						<th>Email</th>
						  						<td class="text-center">{{ $order->order_email }}</td>
						  					</tr>
						  			</tbody>
						        </table>
			              	</div>
			              </div>
			              <div class="col-sm-6 ">
			              	<h4 class="text-center">Doctor Information </h4>
			              	<div class="text-center">
			              		<img  src="{{asset('backend/admin/images/doctors/'.$order->doctor->doctor_image)}}" class="img-circle" height="100px" width="100px">
			              	</div>
			              	<table id="orders" class="table">
						  				<tbody>
						  					<tr>
						  						<th>Doctor Name</th>
						  						<td class="text-center">{{ $order->doctor->doctor_first_name }} {{ $order->doctor->doctor_last_name }}</td>
						  					</tr>
						  					<tr>
						  						<th>Phone</th>
						  						<td class="text-center">{{ $order->doctor->doctor_contact_no}}</td>
						  					</tr>
						  					<tr>
						  						<th>Email</th>
						  						<td class="text-center">{{ $order->doctor->doctor_email }}</td>
						  					</tr>
						  					<tr>
						  						<th>Type Of Doctor </th>
						  						<td class="text-center">{{ $order->doctor->category->category_name }}</td>
						  					</tr>
						  					
						  					</tr>
						  			</tbody>
						        </table>
			              </div>
			            </div>
					</div>
					<div class="box-body" style="background-color: #fffccc">
						<div class="row">
							<div class="col-sm-12">
								
									<h2 style="color: green">Order Schedule Details </h2>
								
								
							    <table id="orders" class="table table-bordered table-striped">
							    	<thead>
							    	  <tr>
							    	  	<th>Medical name</th>
							    	  	<th>Floor Name </th>
							    	  	<th>Room No </th>
							    	  	<th>Day</th>
							    	  	<th>From</th>
							    	  	<th>To</th>
							    	  	<th>Status</th>
							    	  </tr>
							    	</thead>
						  				<tbody>
						  					<tr>
						  						
						  						<td class="text-center">{{ $order->schedule->medical->medical_name }}</td>
						  					
						  						
						  						<td class="text-center">{{ $order->schedule->floor->floor_name }}</td>
						  					
						  						
						  						<td class="text-center">{{ $order->schedule->room->room_number }}</td>
						  					
						  						<td class="text-center">{{ $order->schedule->schedule_day }}</td>
						  					
						  						
						  						<td class="text-center">{{ $order->schedule->start_from }}</td>
						  					
						  						
						  						<td class="text-center">{{ $order->schedule->end_to }}</td>
						  						<td>
						  							 <form  action="{{ route('order_complete',$order->id) }}" method="POST">
													    {{csrf_field()}}
													    @if($order->is_confirmed)
													          <button type="submit" class="float-right btn btn-success btn-sm">Complete</button>
													    @else
													          <button type="submit" class=" float-right btn btn-danger btn-sm">UnComplete</button>
													    @endif
													  </form>
						  						</td>
						  					</tr>
						  			</tbody>
						        </table>

							</div>
							
						</div>
				   </div>
			  </div>
			</div>
		</div>
	</div>
@endsection
