@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<strong class="pull-left" >
			              <h4 style="color: green;font-size: 28px;font-style: italic;"> All Medical Show </h1>
			            </strong>
			            <span class="pull-right">
			              <a href="{{ route('medicals_admin_create')}}" class="btn btn-primary">Create Medical</a>
			            </span>
					</div>
					<div class="box-body">
				      <div class="table-responsive mt-2">
				        <table id="medicals" class="table table-bordered table-striped">
				          <caption>List of medicals</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>medical Name</th>
				  						<th>medical Description</th>
				  						<th>medical Image</th>
				  						<th>medical Address</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($medicals as $medical)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $medical->medical_name }}</td>
				  						<td class="text-center">{{ $medical->medical_description }}</td>
				  						<td class="text-center">
				  							@if($medical->medical_image)
							                  <p>
							                    <img  src="{{asset('backend/admin/images/medicals/'.$medical->medical_image)}}" class="" height="100px" width="100px">
							                  </p>
							                  @else
							                    <p>N/A</p>
							                  @endif
				  						</td>
				  					
				  						<td class="text-center">
				  						    @if($medical->medical_address)
				                  <p>{{ $medical->medical_address}}</p>
				                  @else
				                    <p>N/A</p>
				                  @endif
				  						</td>
				             
				            
				  						<td class="text-center"> <a href="{{route('medicals_admin_show', $medical->id)}}" class="btn btn-primary btn-sm">Show</a>
				  							<a href="{{route('medicals_admin_edit', $medical->id)}}" class="btn btn-warning btn-sm">Edit</a>
				                <a href="#DeleteModal{{ $medical->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$medical->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('medicals_admin_delete', $medical->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>

				      </div>
				       <ul class="pagination">
			                {{ $medicals->links()}}
			              </ul>
				   </div>
			  </div>
			</div>
			
		</div>
	</div>
@endsection
