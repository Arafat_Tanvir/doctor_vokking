@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-8" style="margin-top: 12px;margin-bottom: 12px">
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4>Doctor Create</h1>
			            </div>
					</div>
					<div class="box-body">
					      <form action="{{ route('medicals_admin_update',$medical->id) }}" class="user" method="post" enctype="multipart/form-data">
					         {{ csrf_field()}}
					        <input type="hidden" name="_token" value="{{ csrf_token() }}">
					          <div class="form-group">
					              <label for="medical_name">Medical Name</label>
					              <div class="form-input">
					                  <input type="text" class="form-control" name="medical_name" id="medical_name" placeholder="Enter Doctor First Name" value="{{$medical->medical_name}}" required>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('medical_name')) ? $errors->first('medical_name') : ''}}
					                  </div>
					              </div>
					          </div>
					          

					         
					        

				          <div class="form-group">
				              <label for="medical_description">Medical Description</label>
				              <div class="form-input">
				                  <textarea name="medical_description" cols="4" rows="5" value="{{old('medical_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="medical_description" required>{{$medical->medical_description}}</textarea>
				                  <div class="valid-feedback">
				                    {{ ($errors->has('medical_description')) ? $errors->first('medical_description') : ''}}
				                  </div>
				              </div>
				          </div>

				           <div class="form-group">
				              <label for="medical_address">Medical Address</label>
				              <div class="form-input">
				                  <textarea name="medical_address" cols="4" rows="5" class="form-control" id="medical_address" required>{{$medical->medical_address}}</textarea>
				                  <div class="valid-feedback">
				                    {{ ($errors->has('medical_address')) ? $errors->first('medical_address') : ''}}
				                  </div>
				              </div>
				          </div>

					        <div class="form-group">
					            <label for="medical_image">Medical Image</label>
					              <div class="form-input">
					                  <input type="file" class="form-control " name="medical_image" id="medical_image" placeholder="Enter Category name" value="{{old('medical_image')}}">
					                  <div class="valid-feedback">
					                    {{ ($errors->has('medical_image')) ? $errors->first('medical_image') : ''}}
					                  </div>
					              </div>
					          </div>
					          <button class="btn btn-primary" type="submit">Create Doctor</button>
					      </form>
					</div>
			  </div>
			</div>
			<div class="col-sm-4" style="margin-top: 12px;margin-bottom: 12px">
				<div class="text-center">
					<img  src="{{asset('backend/admin/images/medicals/'.$medical->medical_image)}}" class="img-fluid img-thumbnail" height="300px" width="100%">
				</div>
			</div>
		</div>
	</div>
@endsection
