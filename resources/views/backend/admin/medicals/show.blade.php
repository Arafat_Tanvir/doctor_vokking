@extends('Backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
	    <div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
		    <div class="box">
				<div class="box-header">
					<div class="text-left">
		              <h4> Medical Impormation </h4>
		            </div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="text-center">
								 <img  src="{{asset('backend/admin/images/medicals/'.$medicals->medical_image)}}" class="img-thumbnail" height="340px" width="100%">
							</div>
						</div>
						<div class="col-sm-6">
							<table class="">
								<thead style="font-size: 16px">
									<tr>
										<th>Medical Name : </th>
										<td>{{$medicals->medical_name}}</td>
									</tr>
									<tr>
										<th>Medical Address : </th>
										<td>{{$medicals->medical_address}}</td>
									</tr>
									<tr>
										<th>Medical Description : </th>
										<td>{{$medicals->medical_description}}</td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
		        </div>
		    </div>
        </div>
    </div>
</div>
@endsection