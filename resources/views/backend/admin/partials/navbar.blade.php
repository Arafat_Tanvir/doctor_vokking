<nav class="navbar navbar-white navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <!--  <img src="" class="user-image" alt="User Image"> -->
                    <span class="hidden-xs">Kamrul Hasan</span>
                </a>
                <ul class="dropdown-menu">
                    
                    <li class="user-footer">
                        
                        <div class="pull-right">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();" class="btn btn-warning btn-flat"> {{ __('Sign out') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>

            </li>

        </ul>
    </div>
</nav>