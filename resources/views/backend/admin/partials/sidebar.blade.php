<section class="sidebar">
    <!-- Sidebar user panel -->


    <div class="user-panel">
        <div class="pull-left image">
          <!-- <img src="" class="img-circle" alt="User Image"> -->
        </div>
        <div class="pull-left info">
          <p></p>
          <a href="#"><i class="fa fa-circle text-success"></i></a>
        </div>
      </div>
    <ul class="sidebar-menu" data-widget="tree">
        <li >
            <a href="{{ route('admin.dashboard')}}">
                <i class="fa fa-dashboard" style="font-size:20px;color:white"></i><span style="margin-left: 5px;">Dashboard</span>
                <span class="pull-right-container">
            </span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-bed" style="font-size:20px;color:white"></i>
                <span style="margin-left: 5px;">Docotor</span>
                <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                
               
                <li><a href="{{ route('categories_admin_index')}}"><i class="fa fa-cart-arrow-down" style="font-size:20px;color:white"></i>All Category Show</a></li>

               
               
                <li><a href="{{  route('doctor-admin-index')}}"><i class="fa fa-cart-arrow-down" style="font-size:20px;color:white"></i>All Doctor Show</a></li>
            </ul>
        </li>

         <li class="treeview">
            <a href="#">
                <i class="fa fa-bed" style="font-size:20px;color:white"></i>
                <span style="margin-left: 5px;">Medicals</span>
                <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                
               
                <li><a href="{{ route('medicals_admin_index')}}"><i class="fa fa-cart-arrow-down" style="font-size:20px;color:white"></i>All Medical Show</a></li>
               
               
                <li><a href="{{ route('floors_admin_index')}}"><i class="fa fa-cart-arrow-down" style="font-size:20px;color:white"></i>All Floors Show</a></li>

                
               
                <li><a href="{{ route('rooms_admin_index')}}"><i class="fa fa-cart-arrow-down" style="font-size:20px;color:white"></i>All Room Show</a></li>
                 
                <li><a href="{{ route('schedule-admin-index')}}"><i class="fa fa-eye" style="font-size:20px;color:white"></i> Schedule Show</a></li>
                 
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
               <i class="fa fa-first-order" style="font-size:20px;color:white"></i>
                <span style="margin-left: 5px;">Order</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('order-admin-index')}}"><i class="fa fa-eye" style="font-size:20px;color:white"></i> ALL Order  </a></li>
                

            </ul>
        </li>

    </ul>
</section>