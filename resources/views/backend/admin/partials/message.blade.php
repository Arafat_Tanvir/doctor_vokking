
<div class="text-center" >
	@if(Session::has('success'))
	<div class="alert alert-success Message" id="Message">
		<p>
			{{Session::get('success')}}
		</p>
	</div>
	@endif
</div>
<div class="text-center">
	@if(Session::has('sticky_error'))
	<div class="alert alert-warning Message" id="Message">
		<p>
			{{Session::get('sticky_error')}}
		</p>
	</div>
	@endif
</div>
