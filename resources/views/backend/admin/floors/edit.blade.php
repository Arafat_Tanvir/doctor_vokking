@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-10 col-sm-offset-1" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<div class="text-left">
			              <h4>Doctor Create</h1>
			            </div>
					</div>
					<div class="box-body">
					      <form action="{{ route('floors_admin_update',$floor->id) }}" class="user" method="post" >
					         <input type="hidden" name="_token" value="{{ csrf_token() }}">
					        
					          <div class="form-group">
					              <label for="medical_id">Medical Name</label>
					              <div class="form-input">
					                  <select name="medical_id" id="medical_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
					                          <option value="0" disabled="true" selected="true">===Select medical Name===</option>
					                          @foreach($medicals as $medical)
					                          <option value="{{$medical->id}}" {{ $floor->medical_id == $medical->id ? 'selected' : '' }}>{{$medical->medical_name}}</option>
					                          @endforeach
					                  </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('medical_id')) ? $errors->first('medical_id') : ''}}
					                  </div>
					              </div>
					          </div>

					           <div class="form-group">
					              <label for="floor_name">Floor</label>
					              <div class="form-input">
					              	<select class="form-control" name="floor_name" id="floor_name">
					              		  <option value="0" disabled selected>===Select Floor===</option>
						                  <option value="First Floor" {{ $floor->floor_name == "First Floor" ? 'selected' : ''  }}>First Floor</option>
						                  <option value="Second Floor" {{ $floor->floor_name == "Second Floor" ? 'selected' : ''  }}>Second Floor</option>
						                  <option value="Third Floor" {{ $floor->floor_name == "Third Floor" ? 'selected' : ''  }}>Third Floor</option>
						                  <option value="Fourth Floor" {{ $floor->floor_name == "Fourth Floor" ? 'selected' : ''  }}>Fourth Floor</option>
						                  <option value="Fifth Floor" {{ $floor->floor_name == "Fifth Floor" ? 'selected' : ''  }}>Fifth Floor</option>
						                  <option value="Sixth Floor" {{ $floor->floor_name == "Sixth Floor" ? 'selected' : ''  }}>Sixth Floor</option>
						                  <option value="Seventh Floor" {{ $floor->floor_name == "Seventh Floor" ? 'selected' : ''  }}>Seventh Floor</option>
						                  <option value="Eighth Floor" {{ $floor->floor_name == "Eighth Floor" ? 'selected' : ''  }}>Eighth Floor</option>
						                  <option value="Ninth Floor" {{ $floor->floor_name == "Ninth Floor" ? 'selected' : ''  }}>Ninth Floor</option>
						                  <option value="Tenth Floor" {{ $floor->floor_name == "Tenth Floor" ? 'selected' : ''  }}>Tenth Floor</option>
						                  <option value="Eleventh Floor" {{ $floor->floor_name == "Eleventh Floor" ? 'selected' : ''  }}>Eleventh Floor</option>
					                </select>
					                  <div class="valid-feedback">
					                    {{ ($errors->has('floor_name')) ? $errors->first('floor_name') : ''}}
					                  </div>
					              </div>
					          </div>

					     
					          <button class="btn btn-primary" type="submit">Create Doctor</button>
					      </form>
					</div>
			  </div>
			</div>
		</div>
	</div>
@endsection
