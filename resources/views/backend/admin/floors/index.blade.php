@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')

				<div class="box">
					<div class="box-header">
						<strong class="pull-left" >
			              <h4 style="color: green;font-size: 28px;font-style: italic;"> All Floor Show </h1>
			            </strong>
			            <span class="pull-right">
			              <a href="{{ route('floors_admin_create')}}" class="btn btn-primary">Create Floor</a>
			            </span>
					</div>
					<div class="box-body">
				      <div class="table-responsive mt-2">
				        <table id="floors" class="table table-bordered table-striped">
				          <caption>List of floors</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Medical Name</th>
				  						<th>Floor Name</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($floors as $floor)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $floor->medical->medical_name }}</td>
				  						<td class="text-center">{{ $floor->floor_name }}</td>
				  						
				  						</td>
				             
				            
				  						<td class="text-center"> <a href="{{route('floors_admin_show', $floor->id)}}" class="btn btn-primary btn-sm">Show</a>
				  							<a href="{{route('floors_admin_edit', $floor->id)}}" class="btn btn-warning btn-sm">Edit</a>
				                <a href="#DeleteModal{{ $floor->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$floor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('floors_admin_delete', $floor->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
				      <div class="text-right">
				      <ul class="pagination">
		                {{ $floors->links()}}
		              </ul>
		              </div>
				   </div>
			  </div>
			</div>
		</div>
	</div>
@endsection
