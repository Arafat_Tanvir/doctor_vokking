@extends('backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-6 col-sm-offset-3" style="margin-top: 12px;margin-bottom: 12px">
				@include('backend.admin.partials.message')
				<div class="box">
					<div class="box-header">
						<div class="text-center">
			              <h4>Category Create</h1>
			            </div>
					</div>
					<div class="box-body">
						
						
						<form method="POST" class="user" action="{{ route('categories_admin_store') }}">
				      	{{ csrf_field() }}
				          <div class="form-group">
				              <label for="category_name">Category Name</label>
				              <div class="form-input">
				                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="category_name" id="category_name" placeholder="Enter Category name" value="{{old('category_name')}}" required>
				                  <div class="valid-feedback">
				                    {{ ($errors->has('category_name')) ? $errors->first('category_name') : ''}}
				                  </div>
				              </div>
				          </div>

				          <div class="form-group">
				              <label for="category_description">Category Description</label>
				              <div class="form-input">
				                  <textarea name="category_description" cols="4" rows="5" value="{{old('category_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="category_description"required>{{ old('category_description')}}</textarea>
				                  <div class="valid-feedback">
				                    {{ ($errors->has('category_description')) ? $errors->first('category_description') : ''}}
				                  </div>
				              </div>
				          </div>
				          <button class="btn btn-primary" type="submit">Create Category</button>
				      </form>
				  </div>
			  </div>
			</div>
		</div>
	</div>
@endsection
