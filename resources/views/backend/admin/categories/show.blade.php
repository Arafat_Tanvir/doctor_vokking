@extends('Backend.admin.layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
	    <div class="col-sm-12" style="margin-top: 12px;margin-bottom: 12px">
		    <div class="box">
				<div class="box-header">
					<div class="text-left">
		              <h4> All Category Show </h4>
		            </div>
				</div>
				<div class="box-body">
		        <table class="table table-bordered table-striped">
		            <thead>
	                    <tr>
	                        <th>Category Name</th>
	                        <th>Description</th> 
	                    </tr>
		            </thead>
		            <tbody>
	                    <tr>
	                        <td> {{$categories->category_name}} </td>
	                        <td>{{$categories->category_description }}</td>
	                    </tr>
	                </tbody>
		        </table>
		    </div>
		</div>
        </div>
    </div>
</div>
@endsection
