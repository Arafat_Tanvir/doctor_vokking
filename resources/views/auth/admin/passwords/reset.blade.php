<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>{{ config('app.name', 'Laravel') }}</title>

      <!-- Custom fonts for this template-->
      <link href="{{ asset('Backend/Admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

      <link href="{{ asset('backend/admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

      <!-- Custom styles for this template-->
      <link href="{{ asset('Backend/Admin/css/sb-admin-2.min.css')}}" rel="stylesheet">


      <!-- CkEditor -->
      <script src="{{  asset('/')}}ckeditor/ckeditor.js"></script>
      <script src="{{  asset('/')}}ckeditor/samples/js/sample.js"></script>
      <!--  -->
      <link rel="stylesheet" href="{{  asset('/')}}ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">

      <!--end CkEditor-->


</head>

<body>
  <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="{{ route('/')}}" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Home</span>
                
              </a>
            </li>

          </ul>

        </nav>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-8 col-lg-8 col-md-8 p-5">
        <div class="card o-hidden border-0 shadow-lg my-3" style="background-color: #2F4F4F">
          <div class="card-body p-0">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-white mb-4">Admin reset password</h1>
                  </div>
                     <form method="POST" class="user" action="{{ route('admin.password.reset.post') }}" aria-label="{{ __('Reset Password') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-left">{{ __('Eamil') }}</label>

                            <div class="col-md-8">       
                                <input id="email" type="email" class="form-control form-control-user" name="email" value="{{ $email ?? old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                              </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control form-control-user" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control form-control-user" name="password_confirmation" required>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info btn-user btn-block">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </form>
                </div>
              </div>
            </div>
      </div>

    </div>

  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>