<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>{{ config('app.name', 'Easy Finder') }}</title>

       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">{{ __('Register') }}</div>

                  <div class="card-body">
                      <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}"  enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group row">
                                  <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('first_name') }}</label>

                                  <div class="col-md-6">
                                      <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                      @if ($errors->has('first_name'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('first_name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                            </div>
                            
                            <div class="col-sm-6">
                              <div class="form-group row">
                                  <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('last_name') }}</label>
                                  <div class="col-md-6">
                                      <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                      @if ($errors->has('last_name'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('last_name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group row">
                                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                  <div class="col-md-6">
                                      <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                      @if ($errors->has('email'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                              <div class="col-md-6">
                                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                              <div class="col-md-6">
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('phone') }}</label>

                              <div class="col-md-6">
                                  <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                                  @if ($errors->has('phone'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('phone') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('division_id') ? ' has-error' : '' }}">
                                  <label for="division_id" class="col-md-12 control-label">Division</label>
                                  <div class="col-md-12">
                                      <select name="division_id" id="division_id" class="form-control">
                                          <option value="0" disabled="true" selected="true">===Choose Divisio Name==</option>
                                          @foreach(App\Backend\Admin\Division::orderBy('name','desc')->get(); as $division)
                                          <option value="{{$division->id}}">{{$division->name}}</option>
                                          @endforeach
                                        </select>
                                      @if ($errors->has('division_id'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('division_id') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group{{ $errors->has('district_id') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <label for="district_id" class="control-label">District</label>
                                    <div class="input-group">
                                        <select name="district_id" id="district_id" class="form-control">
                                          <option value="0" disabled="true" selected="true">===Choose District name==</option>
                                        </select>
                                        @if ($errors->has('district_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('district_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                               <div class="form-group{{ $errors->has('upazila_id') ? ' has-error' : '' }}">
                                  <div class="col-md-12">
                                      <label for="upazila_id" class="control-label">Upazila</label>
                                      <div class="input-group">
                                          <select name="upazila_id" id="upazila_id" class="form-control">
                                            <option value="0" disabled="true" selected="true">===Choose Upazila Name==</option>
                                          </select>
                                          @if ($errors->has('upazila_id'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('upazila_id') }}</strong>
                                          </span>
                                          @endif
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group{{ $errors->has('union_id') ? ' has-error' : '' }}">
                                  <div class="col-md-12">
                                      <label for="union_id" class="control-label">Union</label>
                                      <div class="input-group">
                                          <select name="union_id" id="union_id" class="form-control">
                                            <option value="0" disabled="true" selected="true">===Choose Union Name==</option>
                                          </select>
                                          @if ($errors->has('union_id'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('union_id') }}</strong>
                                          </span>
                                          @endif
                                      </div>
                                  </div>
                              </div>
                            </div>
                          </div>


                          <div class="form-group row">
                              <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                              <div class="col-md-6">
                                  <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required autofocus>

                                  @if ($errors->has('address'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('address') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="images" class="col-md-4 col-form-label text-md-right">{{ __('images') }}</label>

                              <div class="col-md-6">
                                  <input id="images" type="file" class="form-control{{ $errors->has('images') ? ' is-invalid' : '' }}" name="images" value="{{ old('images') }}" required autofocus>

                                  @if ($errors->has('images'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('images') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row mb-0">
                              <div class="col-md-6 offset-md-4">
                                  <button type="submit" class="btn btn-primary">
                                      {{ __('Register') }}
                                  </button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>