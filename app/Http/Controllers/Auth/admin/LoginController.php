<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Models\Admin\Admin;
use Illuminate\Http\Request;
use App\Notifications\AdminVerifyRegistration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }


    public function showLoginForm()
    {
        return view('auth.admin.login');
    }


    public function login(Request $request)
    {
        //dd('jfskldf');
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|string',
        ]);
        $admin=Admin::where('email',$request->email)->first();
        //dd($admin);
        if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
        {
            return redirect()->intended(route('admin.dashboard'));
        }else
        {
                session()->flash('stickly','Invaild your password and email!');
                return redirect()->route('admin.login');
        }

    }
    public function logout(Request $request)
    {
        //dd("skfas");
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.login');
    }


}
