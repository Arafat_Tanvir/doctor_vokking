<?php

namespace App\Http\Controllers\Auth\Admin;
use Image;
use App\Models\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\UserVerifyRegistration;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
   protected function register(Request $request)
    {
        //dd($request);
        if(count($request->images)>0)
        {
            //dd($request->images);
            $images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/users/'.$img);
            Image::make($images)->save($location)->resize(300, 300);
        }

        //dd($img);

        $user= User::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
            'phone' => $request->phone,
            'ip_address' => request()->ip(),
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'upazila_id' => $request->upazila_id,
            'union_id' => $request->union_id,
            'address' => $request->address,
            'images'=> $img,
            'remember_token'=>str_random(50),
            'status'=>0,
        ]);
        //dd($user);

        $user->notify(new UserVerifyRegistration($user,$user->remember_token));
        session()->flash('success','A confirmation email has sent to you,please check your email and click to registration confirmed');
        return redirect()->route('/admin/login');
    }
}
