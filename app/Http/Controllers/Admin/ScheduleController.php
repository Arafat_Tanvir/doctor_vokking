<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Doctor;
use App\Models\Schedule;
use App\Models\Medical;

class ScheduleController extends Controller
{
    // all Category Show
  public function index()
	{
	    $schedules =Schedule::orderBy('id','desc')->get();
	    return view('backend.admin.schedules.index',compact('schedules'));
	}
	//Create Category 
	public function create()
  {
      $medicals =Medical::orderBy('id','desc')->get();
    	$doctors =Doctor::orderBy('id','desc')->get();
  	    return view('backend.admin.schedules.create',
          [
    	    	'doctors'=>$doctors,
            'medicals'=>$medicals
  	     ]);
  }

  public function store(Request $request)
  {
        $this->validate($request,[
           'doctor_id'=>'required',
           'floor_id'=>'required',
           'room_id'=>'required',
           'medical_id'=>'required',
           'schedule_day'=>'required',
           'start_from'=>'required',
           'end_to'=>'required',
        ]);
        if($request->sub_room_id)
        {
          $schedules=new Schedule();
          $schedules->doctor_id=$request->doctor_id;
          $schedules->floor_id=$request->floor_id;
          $schedules->room_id=$request->sub_room_id;
          $schedules->medical_id=$request->medical_id;
          $schedules->schedule_day=$request->schedule_day;
          $schedules->start_from=$request->start_from;
          $schedules->end_to=$request->end_to;
          //for minutes
          $start_hour=substr($request->start_from,0, 2);
          $start_minutes=substr($request->start_from, strpos($request->start_from, ':') + 1, 2);
          $new_start_hour=60*$start_hour;
          $input_start_time=$new_start_hour+$start_minutes;


          $end_hour=substr($request->end_to,0, 2);
          $end_minutes=substr($request->end_to, strpos($request->end_to, ':') + 1, 2);
          $new_end_hour=60* $end_hour;
          $input_end_time=$new_end_hour+$end_minutes;

          $room_checked =Schedule::orderBy('id','desc')
          ->where('room_id',$request->room_id)
          ->where('schedule_day',$request->schedule_day)
          ->get();
          

          $doctor_checked =Schedule::orderBy('id','desc')
          ->where('doctor_id',$request->doctor_id)
          ->where('schedule_day',$request->schedule_day)
          ->get();

          if($input_end_time > $input_start_time)
          {
            if($room_checked->count()>0 && $doctor_checked->count()>0)
            {

              foreach ($room_checked as $schedule)
              {
                $before_start_hour=substr($schedule->start_from,0, 2);
                $before_start_minutes=substr($schedule->start_from, strpos($schedule->start_from, ':') + 1, 2);
                $new_before_start_hour=60*$before_start_hour;
                $before_start_time=$new_before_start_hour+$before_start_minutes;


                $before_end_hour=substr($schedule->end_to,0, 2);
                $before_end_minutes=substr($schedule->end_to, strpos($schedule->end_to, ':') + 1, 2);
                $new_before_end_hour=60* $before_end_hour;
                $before_end_time=$new_before_end_hour+$before_end_minutes;

                $overlap = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

                if($overlap==false){
                   break;
                }
              }

              foreach ($doctor_checked as $doctor_check)
              {
                $before_start_hour=substr($doctor_check->start_from,0, 2);
                $before_start_minutes=substr($doctor_check->start_from, strpos($doctor_check->start_from, ':') + 1, 2);
                $new_before_start_hour=60*$before_start_hour;
                $before_start_time=$new_before_start_hour+$before_start_minutes;


                $before_end_hour=substr($doctor_check->end_to,0, 2);
                $before_end_minutes=substr($doctor_check->end_to, strpos($doctor_check->end_to, ':') + 1, 2);
                $new_before_end_hour=60* $before_end_hour;
                $before_end_time=$new_before_end_hour+$before_end_minutes;

                $overlap2 = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

                if($overlap==false){
                   break;
                }
              }
              if($overlap==true && $overlap2==true)
              {
                  $schedules->save();
                  if(!is_null($schedules))
                  {
                      session()->flash('success','Schedule create Successfully!!');
                      return redirect()->route('schedule-admin-index');

                  }else
                  {
                      session()->flash('sticky_error','Some Error Occer!!');
                      return back();
                  }
                }else if($overlap==false && $overlap2==true)
                  {
                      session()->flash('sticky_error','This Room  Not Available');
                      return back();
                  }
                else if ($overlap2==false && $overlap==true){
                  session()->flash('sticky_error','This Doctor  Not Available');
                      return back();
                }else{
                    session()->flash('sticky_error','This Doctor  And Room Not Available');
                      return back();
                  }
              
            }else
            {
              $schedules->save();
              if(!is_null($schedules))
              {
                  session()->flash('success','Schedule create Successfully!!');
                  return redirect()->route('schedule-admin-index');

              }else
              {
                  session()->flash('sticky_error','Some Error Occer!!');
                  return back();
              }
            }
          }else{ 
             session()->flash('sticky_error','Please Select To Time Greater then Start Time');
            return back();
          }

        }else{
          $schedules=new Schedule();
          $schedules->doctor_id=$request->doctor_id;
          $schedules->floor_id=$request->floor_id;
          $schedules->room_id=$request->room_id;
          $schedules->medical_id=$request->medical_id;
          $schedules->schedule_day=$request->schedule_day;
          $schedules->start_from=$request->start_from;
          $schedules->end_to=$request->end_to;
          //for minutes
          $start_hour=substr($request->start_from,0, 2);
          $start_minutes=substr($request->start_from, strpos($request->start_from, ':') + 1, 2);
          $new_start_hour=60*$start_hour;
          $input_start_time=$new_start_hour+$start_minutes;


          $end_hour=substr($request->end_to,0, 2);
          $end_minutes=substr($request->end_to, strpos($request->end_to, ':') + 1, 2);
          $new_end_hour=60* $end_hour;
          $input_end_time=$new_end_hour+$end_minutes;

          $room_checked =Schedule::orderBy('id','desc')
          ->where('room_id',$request->room_id)
          ->where('schedule_day',$request->schedule_day)
          ->get();
          

          $doctor_checked =Schedule::orderBy('id','desc')
          ->where('doctor_id',$request->doctor_id)
          ->where('schedule_day',$request->schedule_day)
          ->get();

          if($input_end_time > $input_start_time)
          {
            if($room_checked->count()>0 && $doctor_checked->count()>0)
            {

              foreach ($room_checked as $schedule)
              {
                $before_start_hour=substr($schedule->start_from,0, 2);
                $before_start_minutes=substr($schedule->start_from, strpos($schedule->start_from, ':') + 1, 2);
                $new_before_start_hour=60*$before_start_hour;
                $before_start_time=$new_before_start_hour+$before_start_minutes;


                $before_end_hour=substr($schedule->end_to,0, 2);
                $before_end_minutes=substr($schedule->end_to, strpos($schedule->end_to, ':') + 1, 2);
                $new_before_end_hour=60* $before_end_hour;
                $before_end_time=$new_before_end_hour+$before_end_minutes;

                $overlap = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

                if($overlap==false){
                   break;
                }
              }

              foreach ($doctor_checked as $doctor_check)
              {
                $before_start_hour=substr($doctor_check->start_from,0, 2);
                $before_start_minutes=substr($doctor_check->start_from, strpos($doctor_check->start_from, ':') + 1, 2);
                $new_before_start_hour=60*$before_start_hour;
                $before_start_time=$new_before_start_hour+$before_start_minutes;


                $before_end_hour=substr($doctor_check->end_to,0, 2);
                $before_end_minutes=substr($doctor_check->end_to, strpos($doctor_check->end_to, ':') + 1, 2);
                $new_before_end_hour=60* $before_end_hour;
                $before_end_time=$new_before_end_hour+$before_end_minutes;

                $overlap2 = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

                if($overlap==false){
                   break;
                }
              }
              if($overlap==true && $overlap2==true)
              {
                  $schedules->save();
                  if(!is_null($schedules))
                  {
                      session()->flash('success','Schedule create Successfully!!');
                      return redirect()->route('schedule-admin-index');

                  }else
                  {
                      session()->flash('sticky_error','Some Error Occer!!');
                      return back();
                  }
                }else if($overlap==false && $overlap2==true)
                  {
                      session()->flash('sticky_error','This Room  Not Available');
                      return back();
                  }
                else if ($overlap2==false && $overlap==true){
                  session()->flash('sticky_error','This Doctor  Not Available');
                      return back();
                }else{
                    session()->flash('sticky_error','This Doctor  And Room Not Available');
                      return back();
                  }
              
            }else
            {
              $schedules->save();
              if(!is_null($schedules))
              {
                  session()->flash('success','Schedule create Successfully!!');
                  return redirect()->route('schedule-admin-index');

              }else
              {
                  session()->flash('sticky_error','Some Error Occer!!');
                  return back();
              }
            }
          }else{ 
             session()->flash('sticky_error','Please Select To Time Greater then Start Time');
            return back();
          }
      }
  }

  public function edit($id)
  {
      $schedule=Schedule::findOrFail($id);
      $medicals =Medical::orderBy('id','desc')->get();
      $doctors =Doctor::orderBy('id','desc')->get();
        return view('backend.admin.schedules.edit',
          [
            'doctors'=>$doctors,
            'medicals'=>$medicals,
            'schedule'=>$schedule
         ]);
  }

  public function update(Request $request,$id)
  {
        $this->validate($request,[
           'doctor_id'=>'required',
           'floor_id'=>'required',
           'room_id'=>'required',
           'medical_id'=>'required',
           'schedule_day'=>'required',
           'start_from'=>'required',
           'end_to'=>'required',
        ]);
        if($request->sub_room_id){
          $schedules=Schedule::findOrFail($id);
          $schedules->doctor_id=$request->doctor_id;
          $schedules->floor_id=$request->floor_id;
          $schedules->room_id=$request->sub_room_id;
          $schedules->medical_id=$request->medical_id;
          $schedules->schedule_day=$request->schedule_day;
          $schedules->start_from=$request->start_from;
          $schedules->end_to=$request->end_to;
          //for minutes
          $start_hour=substr($request->start_from,0, 2);
          $start_minutes=substr($request->start_from, strpos($request->start_from, ':') + 1, 2);
          $new_start_hour=60*$start_hour;
          $input_start_time=$new_start_hour+$start_minutes;


          $end_hour=substr($request->end_to,0, 2);
          $end_minutes=substr($request->end_to, strpos($request->end_to, ':') + 1, 2);
          $new_end_hour=60* $end_hour;
          $input_end_time=$new_end_hour+$end_minutes;

          $room_checked =Schedule::orderBy('id','desc')
          ->where('room_id',$request->room_id)
          ->where('id','!=',$request->id)
          ->where('schedule_day',$request->schedule_day)
          ->get();
          

          $doctor_checked =Schedule::orderBy('id','desc')
          ->where('doctor_id',$request->doctor_id)
          ->where('id','!=',$request->id)
          ->where('schedule_day',$request->schedule_day)
          ->get();

          if($input_end_time > $input_start_time)
          {
            if($room_checked->count()>0 && $doctor_checked->count()>0)
            {

              foreach ($room_checked as $schedule)
              {
                $before_start_hour=substr($schedule->start_from,0, 2);
                $before_start_minutes=substr($schedule->start_from, strpos($schedule->start_from, ':') + 1, 2);
                $new_before_start_hour=60*$before_start_hour;
                $before_start_time=$new_before_start_hour+$before_start_minutes;


                $before_end_hour=substr($schedule->end_to,0, 2);
                $before_end_minutes=substr($schedule->end_to, strpos($schedule->end_to, ':') + 1, 2);
                $new_before_end_hour=60* $before_end_hour;
                $before_end_time=$new_before_end_hour+$before_end_minutes;

                $overlap = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

                if($overlap==false){
                   break;
                }
              }

              foreach ($doctor_checked as $doctor_check)
              {
                $before_start_hour=substr($doctor_check->start_from,0, 2);
                $before_start_minutes=substr($doctor_check->start_from, strpos($doctor_check->start_from, ':') + 1, 2);
                $new_before_start_hour=60*$before_start_hour;
                $before_start_time=$new_before_start_hour+$before_start_minutes;


                $before_end_hour=substr($doctor_check->end_to,0, 2);
                $before_end_minutes=substr($doctor_check->end_to, strpos($doctor_check->end_to, ':') + 1, 2);
                $new_before_end_hour=60* $before_end_hour;
                $before_end_time=$new_before_end_hour+$before_end_minutes;

                $overlap2 = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

                if($overlap==false){
                   break;
                }
              }
              if($overlap==true && $overlap2==true)
              {
                  $schedules->save();
                  if(!is_null($schedules))
                  {
                      session()->flash('success','Schedule Update Successfully!!');
                      return redirect()->route('schedule-admin-index');

                  }else
                  {
                      session()->flash('sticky_error','Some Error Occer!!');
                      return back();
                  }
                }else if($overlap==false && $overlap2==true)
                  {
                      session()->flash('sticky_error','This Room  Not Available');
                      return back();
                  }
                else if ($overlap2==false && $overlap==true){
                  session()->flash('sticky_error','This Doctor  Not Available');
                      return back();
                }else{
                    session()->flash('sticky_error','This Doctor  And Room Not Available');
                      return back();
                  }
            
            }else
            {
              $schedules->save();
              if(!is_null($schedules))
              {
                  session()->flash('success','Schedule Update Successfully!!');
                  return redirect()->route('schedule-admin-index');

              }else
              {
                  session()->flash('sticky_error','Some Error Occer!!');
                  return back();
              }
            }
          }else{ 
             session()->flash('sticky_error','Please Select To Time Greater then Start Time');
            return back();
          }

        }else{
        $schedules=Schedule::findOrFail($id);
        $schedules->doctor_id=$request->doctor_id;
        $schedules->floor_id=$request->floor_id;
        $schedules->room_id=$request->room_id;
        $schedules->medical_id=$request->medical_id;
        $schedules->schedule_day=$request->schedule_day;
        $schedules->start_from=$request->start_from;
        $schedules->end_to=$request->end_to;
        //for minutes
        $start_hour=substr($request->start_from,0, 2);
        $start_minutes=substr($request->start_from, strpos($request->start_from, ':') + 1, 2);
        $new_start_hour=60*$start_hour;
        $input_start_time=$new_start_hour+$start_minutes;


        $end_hour=substr($request->end_to,0, 2);
        $end_minutes=substr($request->end_to, strpos($request->end_to, ':') + 1, 2);
        $new_end_hour=60* $end_hour;
        $input_end_time=$new_end_hour+$end_minutes;

        $room_checked =Schedule::orderBy('id','desc')
        ->where('room_id',$request->room_id)
        ->where('id','!=',$request->id)
        ->where('schedule_day',$request->schedule_day)
        ->get();
        

        $doctor_checked =Schedule::orderBy('id','desc')
        ->where('doctor_id',$request->doctor_id)
        ->where('id','!=',$request->id)
        ->where('schedule_day',$request->schedule_day)
        ->get();

        if($input_end_time > $input_start_time)
        {
          if($room_checked->count()>0 && $doctor_checked->count()>0)
          {

            foreach ($room_checked as $schedule)
            {
              $before_start_hour=substr($schedule->start_from,0, 2);
              $before_start_minutes=substr($schedule->start_from, strpos($schedule->start_from, ':') + 1, 2);
              $new_before_start_hour=60*$before_start_hour;
              $before_start_time=$new_before_start_hour+$before_start_minutes;


              $before_end_hour=substr($schedule->end_to,0, 2);
              $before_end_minutes=substr($schedule->end_to, strpos($schedule->end_to, ':') + 1, 2);
              $new_before_end_hour=60* $before_end_hour;
              $before_end_time=$new_before_end_hour+$before_end_minutes;

              $overlap = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

              if($overlap==false){
                 break;
              }
            }

            foreach ($doctor_checked as $doctor_check)
            {
              $before_start_hour=substr($doctor_check->start_from,0, 2);
              $before_start_minutes=substr($doctor_check->start_from, strpos($doctor_check->start_from, ':') + 1, 2);
              $new_before_start_hour=60*$before_start_hour;
              $before_start_time=$new_before_start_hour+$before_start_minutes;


              $before_end_hour=substr($doctor_check->end_to,0, 2);
              $before_end_minutes=substr($doctor_check->end_to, strpos($doctor_check->end_to, ':') + 1, 2);
              $new_before_end_hour=60* $before_end_hour;
              $before_end_time=$new_before_end_hour+$before_end_minutes;

              $overlap2 = ($input_start_time < $before_start_time && $before_start_time >= $input_end_time || $before_end_time <= $input_start_time && $input_end_time > $before_end_time);

              if($overlap==false){
                 break;
              }
            }
            if($overlap==true && $overlap2==true)
            {
                $schedules->save();
                if(!is_null($schedules))
                {
                    session()->flash('success','Schedule Update Successfully!!');
                    return redirect()->route('schedule-admin-index');

                }else
                {
                    session()->flash('sticky_error','Some Error Occer!!');
                    return back();
                }
              }else if($overlap==false && $overlap2==true)
                {
                    session()->flash('sticky_error','This Room  Not Available');
                    return back();
                }
              else if ($overlap2==false && $overlap==true){
                session()->flash('sticky_error','This Doctor  Not Available');
                    return back();
              }else{
                  session()->flash('sticky_error','This Doctor  And Room Not Available');
                    return back();
                }
            
          }else
          {
            $schedules->save();
            if(!is_null($schedules))
            {
                session()->flash('success','Schedule Update Successfully!!');
                return redirect()->route('schedule-admin-index');

            }else
            {
                session()->flash('sticky_error','Some Error Occer!!');
                return back();
            }
          }
        }else{ 
           session()->flash('sticky_error','Please Select To Time Greater then Start Time');
          return back();
        }
      }
      }

  public function delete($id)
  {
      $schedule=Schedule::find($id);
      if(!is_null($schedule))
      {
            $schedule->delete();
            session()->flash('success','Product has delete Successfully');
            return back();
      }else
      {
          session()->flash('sticky_error','Some Error Occer');
          return back();
      }
  }

}
