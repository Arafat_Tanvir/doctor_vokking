<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Floor;
use App\Models\Medical;
use DB;

class FloorController extends Controller
{
	// all Floor Show
    public function index()
	{
	    $floors =Floor::orderBy('id','desc')->paginate(4);
	    return view('backend.admin.floors.index',compact('floors'));
	}
	  //Create Floor 
	public function create()
    {
    	$medicals =Medical::orderBy('id','desc')->get();
  	    return view('backend.admin.floors.create',[
  	    	'medicals'=>$medicals
  	    ]);
    }
    //load all floors by Medical Id
    public function floors_ajax($id)
    {
        $floors = DB::table('floors')->where('medical_id', $id)->pluck('floor_name','id');
        return json_encode($floors);
    }

	public function store(Request $request)
	{
        $this->validate($request,[
           'floor_name'=>'required|max:255',
           'medical_id'=>'required',
        ]);

        $exits_floor =Floor::where('floor_name',$request->floor_name)
	        ->where('medical_id',$request->medical_id)
	        ->first();



        if ($exits_floor!=null) {
        	session()->flash('sticky_error','This Floor All ready Taken!!');
	        return back();
        }

        $floors=new Floor();
        $floors->floor_name=$request->floor_name;
        $floors->medical_id=$request->medical_id;
        $floors->save();

	    if(!is_null($floors)){

	        session()->flash('success','Floor create Successfully!!');
	        return redirect()->route('floors_admin_create');

	    }else{
	        session()->flash('sticky_error','Some Error Occer!!');
	        return back();
	    }
	}

	public function show($id)
	{
		$floors=Floor::findOrFail($id);
	    return view('backend.admin.floors.show',compact('floors'));
	}
	public function edit($id)
	{
		$medicals =Medical::orderBy('id','desc')->get();
	    $floor = Floor::findOrFail($id);

	    if(!is_null($floor))
	    {
	        return view('backend.admin.floors.edit',[
	        	'floor'=>$floor,
	        	'medicals'=>$medicals
	        ]);
	    }else
	    {
	        return redirect()->route('floors_admin_index');
	    }
	}

	public function update(Request $request, $id)
	{
        $this->validate($request,[
          'floor_name'=>'required',
          'medical_id'=>'required'
        ]);

         $exits_floor =Floor::where('floor_name',$request->floor_name)
	        ->where('medical_id',$request->medical_id)
	        ->first();



        if ($exits_floor!=null) {
        	session()->flash('sticky_error','This Floor All ready Taken!!');
	        return back();
        }

        $floors=Floor::findOrFail($id);
        $floors->floor_name=$request->floor_name;
        $floors->medical_id=$request->medical_id;
        $floors->update();

        if(!is_null($floors))
        {
            session()->flash('success','Floor Update Successfully!!');
            return redirect()->route('floors_admin_index');
        }else
        {
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
	}

	public function delete($id)
	{
	    $floors=Floor::find($id);
	    if(!is_null($floors))
	    {
            $floors->delete();
            session()->flash('success','Product has delete Successfully');
            return back();
	    }else
	    {
	        session()->flash('sticky_error','Some Error Occer');
	        return back();
	    }
	}
}
