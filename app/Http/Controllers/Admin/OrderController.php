<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    public function index(){
    	$orders=Order::orderBy('id','desc')->get();
    	return view('backend.admin.orders.index',[
    		'orders'=>$orders
    	]);
    }

    public function show($id)
	{
		$order=Order::findOrFail($id);
	    return view('backend.admin.orders.show',compact('order'));
	}

    public function order_complete($id)
    {
      $order=Order::findOrFail($id);
      if ($order->is_confirmed) {
        
        $order->is_confirmed=0;
        $order->update();
        session()->flash('sticky_error','Oder Not Confirm');
        return back();
      }else{
        $order->is_confirmed=1;
        $order->update();
        session()->flash('success','Order Successfully Confirm');
        return back();
      }

    }

    public function delete($id)
    {
        $order=Order::find($id);
        if(!is_null($order))
        {
            $order->delete();
            session()->flash('success','Order  delete Successfully');
            return back();
        }else
        {
            session()->flash('sticky_error','Some Error Occer');
            return back();
        }
    }
}
