<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Medical;
use Image;
use File;

class MedicalController extends Controller
{
	// all Medical Show
    public function index()
	{
	    $medicals =Medical::orderBy('id','desc')->paginate(4);
	    return view('backend.admin.medicals.index',compact('medicals'));
	}
	  //Create Medical 
	public function create()
    {
  	    return view('backend.admin.medicals.create');
    }

	public function store(Request $request)
	{
        $this->validate($request,[
           'medical_name'=>'required|max:50|min:2',
        ]);

        $medicals=new Medical();
        $medicals->medical_name=$request->medical_name;
        $medicals->medical_description=$request->medical_description;
        $medicals->medical_address=$request->medical_address;
        if($request->file('medical_image'))
        {
        	$images=$request->file('medical_image');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('backend/admin/images/medicals/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $medicals->medical_image=$img;
        }
        $medicals->save();

	    if(!is_null($medicals)){

	        session()->flash('success','Medical create Successfully!!');
	        return redirect()->route('medicals_admin_index');

	    }else{
	        session()->flash('sticky_error','Some Error Occer!!');
	        return back();
	    }
	}

	public function show($id)
	{
		$medicals=Medical::findOrFail($id);
	    return view('backend.admin.medicals.show',compact('medicals'));
	}
	public function edit($id)
	{
	    $medical = Medical::findOrFail($id);

	    if(!is_null($medical))
	    {
	        return view('backend.admin.medicals.edit',compact('medical'));
	    }else
	    {
	        return redirect()->route('medicals_admin_index');
	    }
	}

	public function update(Request $request, $id)
	{
        $this->validate($request,[
          'medical_name'=>'required|max:50|min:5',
        ]);

        $medicals=Medical::findOrFail($id);
        $medicals->medical_name=$request->medical_name;
        $medicals->medical_description=$request->medical_description;
        $medicals->medical_address=$request->medical_address;
        if($request->file('medical_image'))
        {
        	$images=$request->file('medical_image');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('backend/admin/images/medicals/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $medicals->medical_image=$img;
        }
        $medicals->update();

        if(!is_null($medicals))
        {
            session()->flash('success','Medical Update Successfully!!');
            return redirect()->route('medicals_admin_index');
        }else
        {
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
	}

	public function delete($id)
	{
	    $medicals=Medical::find($id);
	    if(!is_null($medicals))
	    {
            $medicals->delete();
            session()->flash('success','Product has delete Successfully');
            return back();
	    }else
	    {
	        session()->flash('sticky_error','Some Error Occer');
	        return back();
	    }
	}
}
