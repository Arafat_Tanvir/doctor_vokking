<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Doctor;
use App\Models\Category;
use Image;
use File;

class DoctorController extends Controller
{

	public function index()
	{
	    $doctors =Doctor::orderBy('id','desc')->get();
	    return view('backend.admin.doctors.index',compact('doctors'));
	}

    public function create()
    {
    	$categories =Category::orderBy('id','desc')->get();
  	    return view('backend.admin.doctors.create',[
  	    	'categories'=>$categories
  	    ]);
    }

    public function store(Request $request)
	{
        $this->validate($request,[
           'doctor_first_name'=>'required|max:50|min:2',
        ]);

        $doctors=new Doctor();
        $doctors->doctor_first_name=$request->doctor_first_name;
        $doctors->doctor_last_name=$request->doctor_last_name;
        $doctors->doctor_email=$request->doctor_email;
        $doctors->doctor_contact_no=$request->doctor_contact_no;
        $doctors->doctor_gender=$request->doctor_gender;
        $doctors->doctor_designation=$request->doctor_designation;
        $doctors->doctor_education=$request->doctor_education;
        $doctors->category_id=$request->category_id;
        $doctors->doctor_address=$request->doctor_address;

        if($request->file('doctor_image'))
        {
        	$images=$request->file('doctor_image');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('backend/admin/images/doctors/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $doctors->doctor_image=$img;
        }
        $doctors->save();

	    if(!is_null($doctors)){

	        session()->flash('success','Category create Successfully!!');
	        return redirect()->route('doctor-admin-index');

	    }else{
	        session()->flash('sticky_error','Some Error Occer!!');
	        return back();
	    }
	}

    public function show($id)
    {
        $doctor=Doctor::findOrFail($id);
        return view('backend.admin.doctors.show',compact('doctor'));
    }
    public function edit($id)
    {
        $categories =Category::orderBy('id','desc')->get();
        $doctor= Doctor::findOrFail($id);

        if(!is_null($doctor))
        {
            return view('backend.admin.doctors.edit',[
                'categories'=>$categories,
                'doctor'=>$doctor
            ]);
        }else
        {
            return redirect()->route('doctors-admin-index');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'doctor_first_name'=>'required|max:50|min:2',
        ]);

        $doctors=Doctor::findOrFail($id);
        $doctors->doctor_first_name=$request->doctor_first_name;
        $doctors->doctor_last_name=$request->doctor_last_name;
        $doctors->doctor_email=$request->doctor_email;
        $doctors->doctor_contact_no=$request->doctor_contact_no;
        $doctors->doctor_gender=$request->doctor_gender;
        $doctors->doctor_designation=$request->doctor_designation;
        $doctors->doctor_education=$request->doctor_education;
        $doctors->category_id=$request->category_id;
        $doctors->doctor_address=$request->doctor_address;

        if($request->file('doctor_image'))
        {
            $images=$request->file('doctor_image');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('backend/admin/images/doctors/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $doctors->doctor_image=$img;
        }
        $doctors->update();

        if(!is_null($doctors)){

            session()->flash('success','Doctor Update Successfully!!');
            return redirect()->route('doctor-admin-index');

        }else{
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
    }

}
