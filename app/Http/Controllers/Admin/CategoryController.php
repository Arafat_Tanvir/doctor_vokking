<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Category;


class CategoryController extends Controller
{
	// all Category Show
    public function index()
	{
	    $categories =Category::orderBy('id','desc')->paginate(5);
	    return view('backend.admin.categories.index',compact('categories'));
	}
	  //Create Category 
	public function create()
    {
  	    return view('backend.admin.categories.create');
    }

	public function store(Request $request)
	{
        $this->validate($request,[
           'category_name'=>'required|max:50|min:2',
        ]);

        $categories=new Category();
        $categories->category_name=$request->category_name;
        $categories->category_description=$request->category_description;
        $categories->save();

	    if(!is_null($categories)){

	        session()->flash('success','Category create Successfully!!');
	        return redirect()->route('categories_admin_index');

	    }else{
	        session()->flash('sticky_error','Some Error Occer!!');
	        return back();
	    }
	}

	public function show($id)
	{
		$categories=Category::findOrFail($id);
	    return view('backend.admin.categories.show',compact('categories'));
	}
	public function edit($id)
	{
	    $categories = Category::findOrFail($id);

	    if(!is_null($categories))
	    {
	        return view('backend.admin.categories.edit',compact('categories'));
	    }else
	    {
	        return redirect()->route('categories_admin_index');
	    }
	}

	public function update(Request $request, $id)
	{
        $this->validate($request,[
          'category_name'=>'required|max:50|min:5',
        ]);

        $categories=Category::findOrFail($id);
        $categories->category_name=$request->category_name;
        $categories->category_description=$request->category_description;
        $categories->update();

        if(!is_null($categories))
        {
            session()->flash('success','Category Update Successfully!!');
            return redirect()->route('categories_admin_index');
        }else
        {
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
	}

	public function delete($id)
	{
	    $categories=Category::find($id);
	    if(!is_null($categories))
	    {
            $categories->delete();
            session()->flash('success','Product has delete Successfully');
            return back();
	    }else
	    {
	        session()->flash('sticky_error','Some Error Occer');
	        return back();
	    }
	}
}
