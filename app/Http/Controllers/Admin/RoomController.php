<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Medical;
use App\Models\Room;

class RoomController extends Controller
{
	// all room Show
    public function index()
	{
	    $rooms =Room::orderBy('id','desc')->paginate(6);
	    return view('backend.admin.rooms.index',compact('rooms'));
	}
	  //Create room 
	public function create()
    {
    	$medicals =Medical::orderBy('id','desc')->get();
  	    return view('backend.admin.rooms.create',[
  	    	'medicals'=>$medicals
  	    ]);
    }

    public function rooms_ajax($id)
    {
        $rooms = Room::where('floor_id', $id)->where('room_parent_id',null)->pluck('room_number','id');
        return json_encode($rooms);
    }

    

    public function rooms_search_ajax($id)
    {
        $room_seach = Room::where('floor_id', $id)->where('room_parent_id',null)->pluck('room_number','id');
        return json_encode($room_seach);
    }

    public function rooms_sub_ajax($id)
    {
        $room_sub_seach = Room::where('room_parent_id', $id)->pluck('room_number','id');
        return json_encode($room_sub_seach);
    }

    

	public function store(Request $request)
	{
        $this->validate($request,[
           'room_number'=>'required|max:50|min:2',
           'floor_id'=>'required',
        ]);

        $exits_room =Room::where('floor_id',$request->floor_id)
	        ->where('room_number',$request->room_number)
	        ->first();



        if ($exits_room!=null) {
        	session()->flash('sticky_error','This Room All ready Taken!!');
	        return back();
        }


        $rooms=new Room();
        $rooms->room_number=$request->room_number;
        $rooms->room_parent_id=$request->room_parent_id;
        $rooms->floor_id=$request->floor_id;
        $rooms->save();

	    if(!is_null($rooms)){

	        session()->flash('success','room create Successfully!!');
	        return redirect()->route('rooms_admin_create');

	    }else{
	        session()->flash('sticky_error','Some Error Occer!!');
	        return back();
	    }
	}

	public function show($id)
	{
		$rooms=Room::findOrFail($id);
	    return view('backend.admin.rooms.show',compact('rooms'));
	}
	public function edit($id)
	{
	    $room = Room::findOrFail($id);
	    $medicals =Medical::orderBy('id','desc')->get();

	    if(!is_null($room))
	    {
	        return view('backend.admin.rooms.edit',[
	        	'medicals'=>$medicals,
	        	'room'=>$room
	        ]);
	    }else
	    {
	        return redirect()->route('rooms_admin_index');
	    }
	}

	public function update(Request $request, $id)
	{
        $this->validate($request,[
          'room_number'=>'required',
          'floor_id'=>'required',
        ]);

        $exits_room =Room::where('floor_id',$request->floor_id)
	        ->where('room_number',$request->room_number)
	        ->where('id','!=',$request->id)
	        ->first();



        if ($exits_room!=null) {
        	session()->flash('sticky_error','This Room All ready Taken!!');
	        return back();
        }

        $rooms=Room::findOrFail($id);
        $rooms->room_number=$request->room_number;
        $rooms->floor_id=$request->floor_id;
        $rooms->update();

        if(!is_null($rooms))
        {
            session()->flash('success','Room Update Successfully!!');
            return redirect()->route('rooms_admin_index');
        }else
        {
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
	}

	public function delete($id)
	{
	    $rooms=Room::find($id);
	    if(!is_null($rooms))
	    {
            $rooms->delete();
            session()->flash('success','Product has delete Successfully');
            return back();
	    }else
	    {
	        session()->flash('sticky_error','Some Error Occer');
	        return back();
	    }
	}
}
