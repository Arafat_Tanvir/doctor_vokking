<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Doctor;
use App\Models\Schedule;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.home')->with("sel_menu","home");
    }

    public function news()
    {
        return view('frontend.news')->with("sel_menu","news");
    }

    public function services()
    {
        return view('frontend.services')->with("sel_menu","services");
    }

    public function contact()
    {
        return view('frontend.contact')->with("sel_menu","contact");
    }

    public function about_us()
    {
        return view('frontend.about_us')->with("sel_menu","about_us");
    }

    public function doctor_veiw(){
        $doctors =Doctor::orderBy('id','desc')->get();
        
        return view('frontend.booking.index',[
            'doctors'=>$doctors
        ]);
    }

    public function search_schedule(Request $request){
        $doctor_id=$request->doctor_id;
        $doctors =Doctor::orderBy('id','desc')->get();
        $doctor_info =Doctor::orderBy('id','desc')->where('id',$doctor_id)->first();
        $schedule_all =Schedule::orderBy('id','desc')->where('doctor_id',$doctor_id)->get();
        
        return view('frontend.booking.index',[
            'doctor_info'=>$doctor_info,
            'doctors'=>$doctors,
            'schedule_all'=>$schedule_all
        ]);
        
    }
}
