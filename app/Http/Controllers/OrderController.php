<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\Doctor;
use App\Models\Schedule;
use App\Models\Payment;

class OrderController extends Controller
{

	public function request_to_booking($id){
		$payments =Payment::orderBy('id','desc')->get();
		$schedule = Schedule::findOrFail($id);
	    if(!is_null($schedule))
	    {
	        return view('frontend.booking.view',compact('schedule','payments'));
	    }else
	    {
	        return redirect()->route('categories_admin_index');
	    }
	}
    public function order(Request $request){

    	 $this->validate($request,[
           'order_name'=>'required|max:50|min:2',
           'order_email'=>'required',
           'order_phone'=>'required',
           'order_message'=>'required',
           'schedule_id'=>'required',
           'doctor_id'=>'required',
           'payment_id'=>'required',
        ]);
    	 
    	$order=new Order();
		$order->order_name=$request->order_name;
		$order->order_email=$request->order_email;
		$order->order_phone=$request->order_phone;
		$order->order_message=$request->order_message;
        $order->schedule_id=$request->schedule_id;
        $order->doctor_id=$request->doctor_id;
        if($request->payment_id!='Cash'){
	            if($request->transaction_id==NULL || empty($request->transaction_id)){
                session()->flash('sticky_error','Please input transaction id for your payment');
                return back();
            }
        }
        $order->payment_id=Payment::where('short_name',$request->payment_id)->first()->id;
       
        $order->save();

        if(!is_null($order)){
	        Session::flash('success','Order Successfully!');
	        return redirect()->route('public');
		 }else{
		 	Session::flash('errors','Some Error Occour To Order!');
	       return back();
		 }
    }
}
