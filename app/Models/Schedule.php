<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    public function medical(){
	    return $this->belongsTo(Medical::class);
	}

	public function doctor(){
	    return $this->belongsTo(Doctor::class);
	}

	public function floor(){
	    return $this->belongsTo(Floor::class);
	}

	public function room(){
	    return $this->belongsTo(Room::class);
	}
}
