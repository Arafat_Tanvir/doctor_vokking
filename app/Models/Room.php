<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Room;
use App\Models\Floor;

class Room extends Model
{
	
	 public function floor(){
	    return $this->belongsTo(Floor::class);
	}
}
