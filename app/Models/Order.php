<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     public function schedule(){
	    return $this->belongsTo(Schedule::class);
	 }

	 public function doctor(){
	    return $this->belongsTo(Doctor::class);
	 }
}
