<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    public function medical(){
	    return $this->belongsTo(Medical::class);
	}
}
